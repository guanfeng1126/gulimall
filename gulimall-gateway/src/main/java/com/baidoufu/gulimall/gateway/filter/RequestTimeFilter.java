package com.baidoufu.gulimall.gateway.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @className: RequestTimeFilter
 * @description:
 * @author: guanfeng
 * @date: 2020/4/11 23:28
 * @version: V1.0.0
 **/
@Slf4j
public class RequestTimeFilter  implements GlobalFilter, Ordered {

    public static final String REQUEST_TIME_BEGIN="requestTimeBegin";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        exchange.getAttributes().put(REQUEST_TIME_BEGIN,System.currentTimeMillis());
        return chain.filter(exchange).then(
                Mono.fromRunnable(()->{
                    Long startTime=exchange.getAttribute(REQUEST_TIME_BEGIN);
                    if (startTime!=null) {
                        log.info("请求路径是{},消耗时间是:{}ms",exchange.getRequest().getURI().getRawPath(),(System.currentTimeMillis()-startTime));
                    }
                })
        );
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
