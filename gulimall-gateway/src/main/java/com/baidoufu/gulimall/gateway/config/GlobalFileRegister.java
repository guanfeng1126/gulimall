package com.baidoufu.gulimall.gateway.config;

import com.baidoufu.gulimall.gateway.filter.RequestTimeFilter;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @className: GlobalFileRegister
 * @description:
 * @author: guanfeng
 * @date: 2020/4/11 23:58
 * @version: V1.0.0
 **/
@Configuration
public class GlobalFileRegister {

    @Bean
    public GlobalFilter requestTimeFilter() {
        return new RequestTimeFilter();
    }
}
