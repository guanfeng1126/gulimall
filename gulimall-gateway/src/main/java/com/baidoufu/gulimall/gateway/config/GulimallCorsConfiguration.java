package com.baidoufu.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @className: GulimallCorsConfiguration
 * @description: 设置跨域
 * @author: guanfeng
 * @date: 2020/4/19 17:11
 * @version: V1.0.0
 **/
@Configuration
public class GulimallCorsConfiguration {

    @Bean
    public CorsWebFilter corsWebFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //配置跨域
        //允许哪些头信息可以跨域
        corsConfiguration.addAllowedHeader("*");
        //允许哪些方法可以跨域
        corsConfiguration.addAllowedMethod("*");
        //允许哪些请求来源可以跨域
        corsConfiguration.addAllowedOrigin("*");
        //是否可以允许携带cookie跨域
        corsConfiguration.setAllowCredentials(true);
        source.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsWebFilter(source);

    }
}
