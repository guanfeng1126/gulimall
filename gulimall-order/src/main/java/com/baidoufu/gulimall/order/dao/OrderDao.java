package com.baidoufu.gulimall.order.dao;

import com.baidoufu.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 14:32:40
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
