package com.baidoufu.common.utils;

import java.io.Serializable;

/**
 * @className: Result
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 19:02
 * @version: V1.0.0
 **/
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -5629197194607518311L;
    private Integer code;

    private String msg;

    private T data;

    public static <T> Result<T> succ(T data) {
        Result result = new Result(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMsg());
        result.setData(data);
        return result;
    }

    public Result() {
    }

    public Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
