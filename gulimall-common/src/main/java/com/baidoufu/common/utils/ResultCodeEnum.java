package com.baidoufu.common.utils;

/**
 * @className: ResultCodeEnum
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 19:04
 * @version: V1.0.0
 **/
public enum ResultCodeEnum {
    /**
     *
     */
    SUCCESS(200, "success"),
    FAIL(500, "fail"),
    PRODUCT_UP_EXCEPTION(11000,"商品上架失败");

    private Integer code;

    private String msg;

     ResultCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
