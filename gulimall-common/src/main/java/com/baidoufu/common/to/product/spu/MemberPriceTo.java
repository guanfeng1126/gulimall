/**
  * Copyright 2020 bejson.com 
  */
package com.baidoufu.common.to.product.spu;


import java.io.Serializable;
import java.math.BigDecimal;


public class MemberPriceTo implements Serializable {

    private static final long serialVersionUID = -4517267502523728528L;
    private Long id;
    private String name;
    /**
     * 会员价格
     */
    private BigDecimal price;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}