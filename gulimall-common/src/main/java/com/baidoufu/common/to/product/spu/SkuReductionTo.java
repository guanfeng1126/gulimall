package com.baidoufu.common.to.product.spu;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @className: SpuReductionTo
 * @description:
 * @author: guanfeng
 * @date: 2020/6/4 11:35
 * @version: V1.0.0
 **/
public class SkuReductionTo implements Serializable {

    private static final long serialVersionUID = 9188147467368674011L;
    /**
     * sku的id
     */
    private Long skuId;
    /**
     * 满几件
     */
    private Integer fullCount;
    /**
     * 打几折
     */
    private BigDecimal discount;
    private Integer countStatus;
    /**
     * 满多少
     */
    private BigDecimal fullPrice;
    /**
     * 减多少
     */
    private BigDecimal reducePrice;
    private Integer priceStatus;

    private List<MemberPriceTo> memberPrice;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public List<MemberPriceTo> getMemberPrice() {
        return memberPrice;
    }

    public void setMemberPrice(List<MemberPriceTo> memberPrice) {
        this.memberPrice = memberPrice;
    }

    public Integer getFullCount() {
        return fullCount;
    }

    public void setFullCount(Integer fullCount) {
        this.fullCount = fullCount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Integer getCountStatus() {
        return countStatus;
    }

    public void setCountStatus(Integer countStatus) {
        this.countStatus = countStatus;
    }

    public BigDecimal getFullPrice() {
        return fullPrice;
    }

    public void setFullPrice(BigDecimal fullPrice) {
        this.fullPrice = fullPrice;
    }

    public BigDecimal getReducePrice() {
        return reducePrice;
    }

    public void setReducePrice(BigDecimal reducePrice) {
        this.reducePrice = reducePrice;
    }

    public Integer getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(Integer priceStatus) {
        this.priceStatus = priceStatus;
    }
}
