package com.baidoufu.common.to.product.spu;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @className: SpuBoundTo
 * @description:
 * @author: guanfeng
 * @date: 2020/6/3 23:10
 * @version: V1.0.0
 **/
public class SpuBoundTo implements Serializable {

    private static final long serialVersionUID = -8878630563197989647L;

    /**
     * spuid
     */
    private Long spuId;
    /**
     * 购物积分
     */
    private BigDecimal buyBounds;
    /**
     * 成长积分
     */
    private BigDecimal growBounds;

    public Long getSpuId() {
        return spuId;
    }

    public void setSpuId(Long spuId) {
        this.spuId = spuId;
    }

    public BigDecimal getBuyBounds() {
        return buyBounds;
    }

    public void setBuyBounds(BigDecimal buyBounds) {
        this.buyBounds = buyBounds;
    }

    public BigDecimal getGrowBounds() {
        return growBounds;
    }

    public void setGrowBounds(BigDecimal growBounds) {
        this.growBounds = growBounds;
    }
}
