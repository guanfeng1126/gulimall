package com.baidoufu.common.valid;

import com.google.common.collect.Sets;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;

/**
 * @className: ListValueConstraintValidator
 * @description:
 * @author: guanfeng
 * @date: 2020/5/20 16:40
 * @version: V1.0.0
 **/
public class ListValueConstraintValidator implements ConstraintValidator<ListValue,Integer> {

    private Set<Integer> set= Sets.newHashSet();
    /**
     * 初始化方法
     * @param constraintAnnotation
     */
    @Override
    public void initialize(ListValue constraintAnnotation) {
        int[] vals=constraintAnnotation.value();
        for (int val : vals) {
            set.add(val);
        }
    }

    /**
     * 判断校验是否成功
     * @param value 需要校验的值
     * @param context
     * @return
     */
    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        return set.contains(value);
    }
}
