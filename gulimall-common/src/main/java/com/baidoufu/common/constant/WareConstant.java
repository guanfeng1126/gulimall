package com.baidoufu.common.constant;

/**
 * @className: WareConstant
 * @description: 仓库服务常用类
 * @author: guanfeng
 * @date: 2020/6/6 13:57
 * @version: V1.0.0
 **/
public class WareConstant {

    /**
     * 采购需求枚举类
     */
    public enum PurchaseStatusEnum{
        /**
         * 新建
         */
        CREATE(0,"新建"),
        ASSIGNED(1,"已分配"),
        RECEIVE(2,"已领取"),
        FINISH(3,"已完成"),
        HASERROR(4,"有异常"),

        ;


        PurchaseStatusEnum(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        private Integer code;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
    /**
     * 采购单求枚举类
     */
    public enum PurchaseDetailStatusEnum{
        /**
         * 新建
         */
        CREATE(0,"新建"),
        ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),
        FINISH(3,"已完成"),
        HASERROR(4,"采购失败"),

        ;


        PurchaseDetailStatusEnum(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        private Integer code;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

}
