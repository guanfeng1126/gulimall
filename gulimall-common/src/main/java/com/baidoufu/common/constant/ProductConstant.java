package com.baidoufu.common.constant;

import jdk.nashorn.internal.objects.annotations.Getter;

/**
 * @className: ProductConstant
 * @description: product的常量类
 * @author: guanfeng
 * @date: 2020/5/31 22:15
 * @version: V1.0.0
 **/
public class ProductConstant {
    /**
     * 通用0字段
     */
    public static final String COMMON_ZERO = "0";

    public static final String CATEGORY_JSON_CACHE="catalogJson";

    /**
     * 属性枚举
     */
    public enum AttrEnum {
        /**
         * 基本属性
         */
        ATTR_TYPE_BASE(1, "base"),
        /**
         * 销售属性
         */
        ATTR_TYPE_SALE(0, "sale");

        AttrEnum(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        private Integer code;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    /**
     * 商品状态枚举
     */
    public enum StatusEnum {
        /**
         * 基本属性
         */
        NEW_SPU(0, "新建"),
        /**
         * 销售属性
         */
        SPU_UP(1, "商品上架"),
        SPU_DOWN(2, "商品下架");

        StatusEnum(Integer code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        private Integer code;

        private String msg;

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
