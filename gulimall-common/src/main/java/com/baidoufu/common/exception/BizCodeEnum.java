package com.baidoufu.common.exception;

/**
 * @className: BizCodeEnum
 * @description:
 * @author: guanfeng
 * @date: 2020/5/19 15:01
 * @version: V1.0.0
 **/

public enum BizCodeEnum {
    /**
     *
     */
    UNKNOW_EXCEPTION(10000,"系统位置异常"),
    VALID_EXCEPTION(10001,"参数格式校验失败"),
    PRODUCT_UP_EXCEPTION(11000,"商品上架失败");

    private Integer code;

    private String msg;

    BizCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
