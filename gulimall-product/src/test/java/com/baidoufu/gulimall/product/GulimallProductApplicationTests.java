package com.baidoufu.gulimall.product;

//import com.aliyun.oss.OSSClient;

import com.baidoufu.gulimall.product.entity.BrandEntity;
import com.baidoufu.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class GulimallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Autowired
    StringRedisTemplate redisTemplate;

    @Autowired
    RedissonClient redissonClient;

    //@Autowired
    //OSSClient ossClient;
    @Test
    public void testRedissonClient() {
        log.info("redissonClient={}", redissonClient);
    }


    @Test
    public void testRedisTemplate() {
        ValueOperations<String, String> ops = redisTemplate.opsForValue();
        ops.set("hello", "word_" + UUID.randomUUID().toString());

    }

    @Test
    public void testUploadByoss() throws FileNotFoundException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        //String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
// 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        // String accessKeyId = "<yourAccessKeyId>";
        //String accessKeySecret = "<yourAccessKeySecret>";

// 创建OSSClient实例。
        // OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

// 上传文件流。
        InputStream inputStream = new FileInputStream("D:\\tools\\image\\feiqiu.jpg");
        //ossClient.putObject("gulimall-brand", "feiqiu.jpg", inputStream);

// 关闭OSSClient。
        //ossClient.shutdown();
        System.out.println("上传完毕。。。");
    }

    @Test
    public void contextLoads() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setDescript("测试新增");
        brandEntity.setName("华为");
        boolean res = brandService.save(brandEntity);

        System.out.println("插入成功");
    }

    @Test
    public void testUpdateById() {
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setBrandId(2L);
        brandEntity.setDescript("华为测试修改描述");
        brandService.updateById(brandEntity);
    }

    @Test
    public void testList() {
        LambdaQueryWrapper<BrandEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(BrandEntity::getBrandId, 2L);

        List<BrandEntity> brandEntityList = brandService.list(queryWrapper);
        brandEntityList.stream().forEach(brandEntity -> {
            System.out.println(brandEntity.toString());
        });
    }

}
