package com.baidoufu.gulimall.product.service.impl;

import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;
import com.baidoufu.gulimall.product.dto.AttrGroupRelationDTO;
import com.baidoufu.gulimall.product.util.MapStructBeanUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.baidoufu.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.baidoufu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.baidoufu.gulimall.product.service.AttrAttrgroupRelationService;


@Service("attrAttrgroupRelationService")
@Slf4j
@RequiredArgsConstructor
public class  AttrAttrgroupRelationServiceImpl extends ServiceImpl<AttrAttrgroupRelationDao, AttrAttrgroupRelationEntity> implements AttrAttrgroupRelationService {
    private final MapStructBeanUtils beanUtils;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrAttrgroupRelationEntity> page = this.page(
                new Query<AttrAttrgroupRelationEntity>().getPage(params),
                new QueryWrapper<AttrAttrgroupRelationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void addRelationAttr(List<AttrGroupRelationDTO> relationDTOS) {
        List<AttrAttrgroupRelationEntity> relationEntities = relationDTOS.stream().
                map(obj -> beanUtils.dtoToAttrGroupRelationEntity(obj)).collect(Collectors.toList());
        this.saveBatch(relationEntities);
    }

}