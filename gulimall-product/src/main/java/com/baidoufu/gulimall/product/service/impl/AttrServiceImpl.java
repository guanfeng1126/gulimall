package com.baidoufu.gulimall.product.service.impl;

import com.baidoufu.common.constant.ProductConstant;
import com.baidoufu.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.baidoufu.gulimall.product.dao.AttrGroupDao;
import com.baidoufu.gulimall.product.dao.CategoryDao;
import com.baidoufu.gulimall.product.dto.AttrDTO;
import com.baidoufu.gulimall.product.entity.*;
import com.baidoufu.gulimall.product.service.CategoryService;
import com.baidoufu.gulimall.product.util.MapStructBeanUtils;
import com.baidoufu.gulimall.product.vo.AttrRespVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.product.dao.AttrDao;
import com.baidoufu.gulimall.product.service.AttrService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("attrService")
@Slf4j
@RequiredArgsConstructor
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    private final MapStructBeanUtils beanUtils;

    private final AttrAttrgroupRelationDao relationDao;

    private final CategoryDao categoryDao;

    private final AttrGroupDao attrGroupDao;

    private final CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveAttr(AttrDTO attrDTO) {
        AttrEntity attrEntity = beanUtils.attrDTO2Entity(attrDTO);
        //保存基本数据
        this.save(attrEntity);
        //保存关联关系表,只有在基本属性的时候才保存关联关系，销售属性不需进行下面操作
        if (ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode().equals(attrDTO.getAttrType())
                && !Objects.isNull(attrDTO.getAttrGroupId())) {
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
            attrAttrgroupRelationEntity.setAttrGroupId(attrDTO.getAttrGroupId());
            attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());
            relationDao.insert(attrAttrgroupRelationEntity);
        }

    }

    @Override
    public PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String type) {
        //设置查询基本属性还是销售属性
        LambdaQueryWrapper<AttrEntity> queryWrapper = new LambdaQueryWrapper<AttrEntity>().eq(AttrEntity::getAttrType,
                ProductConstant.AttrEnum.ATTR_TYPE_BASE.getMsg().equalsIgnoreCase(type) ? ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode() : ProductConstant.AttrEnum.ATTR_TYPE_SALE.getCode());
        if (catelogId != 0) {
            queryWrapper.eq(AttrEntity::getCatelogId, catelogId);
        }
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            //attr_id= or att_name like
            queryWrapper.and((wrapper) -> {
                wrapper.eq(AttrEntity::getAttrId, key).or().like(AttrEntity::getAttrName, key);
            });
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), queryWrapper);
        PageUtils pages = new PageUtils(page);
        List<AttrEntity> records = page.getRecords();
        //查询所属分组名称和分类名称
        List<AttrRespVO> attrRespVOList = records.stream().map(attrtEntity -> {
            AttrRespVO attrRespVO = beanUtils.attrToVo(attrtEntity);
            if (ProductConstant.AttrEnum.ATTR_TYPE_BASE.getMsg().equalsIgnoreCase(type)) {
                AttrAttrgroupRelationEntity relationEntity = relationDao.
                        selectOne(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().
                                eq(AttrAttrgroupRelationEntity::getAttrId, attrtEntity.getAttrId()));
                if (!Objects.isNull(relationEntity) && !Objects.isNull(relationEntity.getAttrGroupId())) {
                    AttrGroupEntity groupEntity = attrGroupDao.selectById(relationEntity.getAttrGroupId());
                    attrRespVO.setGroupName(groupEntity.getAttrGroupName());
                }
            }

            CategoryEntity categoryEntity = categoryDao.selectById(attrtEntity.getCatelogId());
            if (!Objects.isNull(categoryEntity)) {
                attrRespVO.setCatelogName(categoryEntity.getName());
            }

            return attrRespVO;
        }).collect(Collectors.toList());
        pages.setList(attrRespVOList);
        return pages;
    }

    @Override
    public AttrRespVO getAttrInfo(Long attrId) {
        AttrEntity attrEntity = this.getById(attrId);
        //基本信息转换
        AttrRespVO attrRespVO = beanUtils.attrToVo(attrEntity);
        //1、设置分组信息，基本属性设置，销售属性直接跳过
        if (ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode().equals(attrEntity.getAttrType())) {
            AttrAttrgroupRelationEntity relationEntity = relationDao.selectOne(
                    new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().
                            eq(AttrAttrgroupRelationEntity::getAttrId, attrId));
            if (!Objects.isNull(relationEntity)) {
                attrRespVO.setAttrGroupId(relationEntity.getAttrGroupId());
                AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(relationEntity.getAttrGroupId());
                if (!Objects.isNull(attrGroupEntity)) {
                    attrRespVO.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }
        }
        //2、设置分类信息
        Long[] cateLogPath = categoryService.findCateLogPath(attrEntity.getCatelogId());
        attrRespVO.setCatelogPath(cateLogPath);
        //设置分类名称
        CategoryEntity categoryEntity = categoryDao.selectById(attrEntity.getCatelogId());
        if (!Objects.isNull(categoryEntity)) {
            attrRespVO.setCatelogName(categoryEntity.getName());
        }
        return attrRespVO;
    }

    @Override
    public void updateAttrDetail(AttrDTO attrDTO) {
        //1、保存attr表
        AttrEntity attrEntity = beanUtils.attrDTO2Entity(attrDTO);
        this.updateById(attrEntity);
        //2、判断属性和属性分组关联表中是否存在对应关联关系，如果是基本属性才进行关联关系修改
        if (ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode().equals(attrEntity.getAttrType())) {
            Integer count = relationDao.selectCount(
                    new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().
                            eq(AttrAttrgroupRelationEntity::getAttrId, attrEntity.getAttrId()));
            AttrAttrgroupRelationEntity relationEntity = new AttrAttrgroupRelationEntity();
            relationEntity.setAttrId(attrEntity.getAttrId());
            relationEntity.setAttrId(attrDTO.getAttrGroupId());

            //存在关系，更新即可
            if (count > 0) {
                relationDao.update(relationEntity,
                        new LambdaUpdateWrapper<AttrAttrgroupRelationEntity>().
                                eq(AttrAttrgroupRelationEntity::getAttrId, relationEntity.getAttrId()));
                //不存在则新增
            } else {
                relationDao.insert(relationEntity);
            }
        }


    }

    @Override
    public List<Long> selectSearchAttrs(List<Long> attrIds) {

        if (CollectionUtils.isEmpty(attrIds)) {
            //todo 异常处理
        }

        return baseMapper.selectSearchAttrs(attrIds);

    }

}