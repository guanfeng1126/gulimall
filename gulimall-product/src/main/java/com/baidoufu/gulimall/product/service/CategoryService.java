package com.baidoufu.gulimall.product.service;

import com.baidoufu.gulimall.product.vo.Catelog2VO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.entity.CategoryEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询分类以及子类
     *
     * @return
     */
    List<CategoryEntity> listWithTree();

    void removeMenusByIds(List<Long> catIds);

    /**
     * 根据categoryId查找完整路径
     *
     * @param categoryId
     * @return
     */
    Long[] findCateLogPath(Long categoryId);

    /**
     * 更新category表的时候，更新其他表的冗余字段
     *
     * @param category
     */
    void updateCascade(CategoryEntity category);

    /**
     * 查询所有的一级分类
     *
     * @return
     */
    List<CategoryEntity> getLevel1Category();

    /**
     * 返回首页的分类数据格式
     *
     * @return
     */
    Map<String, List<Catelog2VO>> getCatalogJson();
}

