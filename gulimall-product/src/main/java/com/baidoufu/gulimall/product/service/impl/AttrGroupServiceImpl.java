package com.baidoufu.gulimall.product.service.impl;

import com.baidoufu.common.constant.ProductConstant;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;
import com.baidoufu.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.baidoufu.gulimall.product.dao.AttrDao;
import com.baidoufu.gulimall.product.dao.AttrGroupDao;
import com.baidoufu.gulimall.product.dto.AttrGroupRelationDTO;
import com.baidoufu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.baidoufu.gulimall.product.entity.AttrEntity;
import com.baidoufu.gulimall.product.entity.AttrGroupEntity;
import com.baidoufu.gulimall.product.service.AttrGroupService;
import com.baidoufu.gulimall.product.service.AttrService;
import com.baidoufu.gulimall.product.util.MapStructBeanUtils;
import com.baidoufu.gulimall.product.vo.AttrGroupWithAttrsVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author baidoufu
 */
@Service("attrGroupService")
@Slf4j
@RequiredArgsConstructor
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    private final AttrAttrgroupRelationDao relationDao;

    private final AttrDao attrDao;

    private final MapStructBeanUtils beanUtils;

    private final AttrService attrService;

    private final AttrGroupDao groupDao;


    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        String key = (String) params.get("key");
        LambdaQueryWrapper<AttrGroupEntity> queryWrapper = new LambdaQueryWrapper<AttrGroupEntity>();
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.and((obj) -> {
                obj.eq(AttrGroupEntity::getAttrGroupId, key).or().like(AttrGroupEntity::getAttrGroupName, key);
            });
        }
        if (catelogId == 0) {
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), queryWrapper);
            return new PageUtils(page);
        } else {
            queryWrapper.eq(AttrGroupEntity::getCatelogId, catelogId);
            IPage<AttrGroupEntity> page = this.page(new Query<AttrGroupEntity>().getPage(params), queryWrapper);
            return new PageUtils(page);

        }
    }

    @Override
    public List<AttrEntity> getAttrRelationList(Long attrgroupId) {
        //根据属性分组id查询在属性-属性分组关联表中的所有关联数据
        List<AttrAttrgroupRelationEntity> relationEntities = relationDao.selectList(
                new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().
                        eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrgroupId));
        //吧所有的属性id提取出来
        List<Long> attrIds = relationEntities.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());
        //查询所有的属性返回
        if (!CollectionUtils.isEmpty(attrIds)) {
            return attrDao.selectBatchIds(attrIds);
        }

        return null;
    }

    @Override
    public void deleteBatchRelation(List<AttrGroupRelationDTO> relationDTOS) {
        List<AttrAttrgroupRelationEntity> entities = relationDTOS.stream().map(dto -> beanUtils.dtoToAttrGroupRelationEntity(dto)).collect(Collectors.toList());
        relationDao.deleteBatchRelation(entities);
    }

    @Override
    public PageUtils listNoRelationAttr(Long attrgroupId, Map<String, Object> params) {
        //1、当前分组只能关联自己所属的分类里面的属性
        //查询到当前分组
        AttrGroupEntity groupEntity = groupDao.selectById(attrgroupId);
        //拿到当前分组所在的分类
        Long categoryId = groupEntity.getCatelogId();
        //2、当前分组只能关联当前分类下面的分组没有关联的属性
        //查询目前分类下面的所包含的分组
        List<AttrGroupEntity> dbGroups = groupDao.selectList(new LambdaQueryWrapper<AttrGroupEntity>().
                eq(AttrGroupEntity::getCatelogId, categoryId));
        //获取所有分组的groupId集合
        List<Long> groupIds = dbGroups.stream().map(AttrGroupEntity::getAttrGroupId).collect(Collectors.toList());

        //通过分组id集合查询 属性-属性分组关联关系表，查出已经被当前分类下面分组已经关联的属性
        List<AttrAttrgroupRelationEntity> relationEntityList = relationDao.selectList(
                new LambdaQueryWrapper<AttrAttrgroupRelationEntity>().
                        in(AttrAttrgroupRelationEntity::getAttrGroupId, groupIds));
        //遍历拿到当前已经被引用的属性id集合
        List<Long> attrIds = relationEntityList.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());
        //从当前分类下面的属性中拿到没有被关联的属性，只查询基本属性
        LambdaQueryWrapper<AttrEntity> queryWrapper = new LambdaQueryWrapper<AttrEntity>().
                eq(AttrEntity::getCatelogId, categoryId).
                eq(AttrEntity::getAttrType, ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if (!CollectionUtils.isEmpty(attrIds)) {
            queryWrapper.notIn(AttrEntity::getAttrId, attrIds);
        }
        //判断模糊搜索
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.and(query -> {
                query.eq(AttrEntity::getAttrId, key).or().like(AttrEntity::getAttrName, key);
            });
        }
        IPage<AttrEntity> page = attrDao.selectPage(new Query<AttrEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);
    }

    @Override
    public List<AttrGroupWithAttrsVO> getAttrGroupWithAttrsByCatelogId(Long catelogId) {
        //查询分组信息
        List<AttrGroupEntity> groups = groupDao.selectList(new LambdaQueryWrapper<AttrGroupEntity>().eq(AttrGroupEntity::getCatelogId, catelogId));
        List<AttrGroupWithAttrsVO> groupWithAttrsVOS = groups.stream().map(group -> {
            AttrGroupWithAttrsVO groupWithAttrsVO = beanUtils.attrGroupToVo(group);
            List<AttrEntity> attrList = getAttrRelationList(group.getAttrGroupId());
            groupWithAttrsVO.setAttrs(attrList);
            return groupWithAttrsVO;
        }).collect(Collectors.toList());
        return groupWithAttrsVOS;
    }


}