package com.baidoufu.gulimall.product.service;

import com.baidoufu.gulimall.product.dto.spu.SpuSaveDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:17
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 商品发布业务逻辑
     * @param spuInfo
     */
    void saveSpuInfo(SpuSaveDTO spuInfo);

    /**
     * spu基本信息保存
     * @param spuInfoEntity
     */
    void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity);

    /**
     * 分页查询
     * @param params
     * @return
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 商品上架功能
     * @param spuId
     */
    void up(Long spuId);
}

