package com.baidoufu.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

/**
 * @author baidoufu
 *
 * 5自定义校验器
 *  1）、编写一个自定义校验注解
 *  2）、编写一个自定义校验注器
 *  3）、关联自定义注解和自定义校验器器
 */
@SpringBootApplication
@MapperScan("com.baidoufu.gulimall.product.dao")
@EnableDiscoveryClient
@EnableCaching
@EnableFeignClients(basePackages = "com.baidoufu.gulimall.product.fegin")
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
