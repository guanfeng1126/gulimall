package com.baidoufu.gulimall.product.service.impl;

import com.baidoufu.common.constant.ProductConstant;
import com.baidoufu.common.to.es.SkuEsModel;
import com.baidoufu.common.to.product.spu.SkuReductionTo;
import com.baidoufu.common.to.product.spu.SpuBoundTo;
import com.baidoufu.common.utils.R;
import com.baidoufu.common.utils.Result;
import com.baidoufu.gulimall.product.dto.spu.*;
import com.baidoufu.gulimall.product.entity.*;
import com.baidoufu.gulimall.product.fegin.CouponFeginService;
import com.baidoufu.gulimall.product.fegin.SearchFgeinService;
import com.baidoufu.gulimall.product.fegin.WareFeginService;
import com.baidoufu.gulimall.product.service.*;
import com.baidoufu.gulimall.product.util.MapStructBeanUtils;
import com.baidoufu.gulimall.product.vo.SkuHasStockVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.product.dao.SpuInfoDao;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("spuInfoService")
@Slf4j
@RequiredArgsConstructor
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    private final MapStructBeanUtils beanUtils;

    private final SpuInfoDescService spuInfoDescService;

    private final SpuImagesService spuImagesService;

    private final AttrService attrService;

    private final ProductAttrValueService attrValueService;

    private final SkuInfoService skuInfoService;

    private final SkuImagesService skuImagesService;

    private final SkuSaleAttrValueService skuSaleAttrValueService;

    private final CouponFeginService couponFeginService;

    private final BrandService brandService;

    private final CategoryService categoryService;

    private final WareFeginService wareFeginService;

    private final SearchFgeinService searchFgeinService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }

    /**
     * todo 分布式事务 后续解决，包括异常处理
     *
     * @param spuInfoDto
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSpuInfo(SpuSaveDTO spuInfoDto) {
        //1、保存spu的基本信息；pms_spu_info
        SpuInfoEntity infoEntity = beanUtils.spuInfoDtoToEntity(spuInfoDto);
        infoEntity.setCreateTime(new Date());
        infoEntity.setUpdateTime(new Date());
        this.saveBaseSpuInfo(infoEntity);

        //2、保存spu的描述图片; pms_spu_info_desc
        List<String> decript = spuInfoDto.getDecript();
        SpuInfoDescEntity descEntity = new SpuInfoDescEntity();
        descEntity.setSpuId(infoEntity.getId());
        descEntity.setDecript(String.join(",", decript));
        spuInfoDescService.saveSpuInfoDesc(descEntity);

        //3、保存spu图片集;psm_spu_images
        List<String> images = spuInfoDto.getImages();
        spuImagesService.saveImage(infoEntity.getId(), images);
        //4、保存spu的规格参数；pms_product_attr_value
        List<BaseAttrsDTO> baseAttrs = spuInfoDto.getBaseAttrs();
        List<ProductAttrValueEntity> attrValueEntities = baseAttrs.stream().map(attr -> {
            ProductAttrValueEntity valueEntity = new ProductAttrValueEntity();
            valueEntity.setAttrId(attr.getAttrId());
            AttrEntity attrEntity = attrService.getById(attr.getAttrId());
            valueEntity.setAttrName(attrEntity.getAttrName());
            valueEntity.setAttrValue(attr.getAttrValues());
            valueEntity.setQuickShow(attr.getShowDesc());
            valueEntity.setSpuId(infoEntity.getId());
            return valueEntity;
        }).collect(Collectors.toList());
        attrValueService.saveProductAttr(attrValueEntities);


        //5、保存spu的积分信息：gulimall_sms->sms_spu_bounds
        BoundsDTO bounds = spuInfoDto.getBounds();
        SpuBoundTo boundTo = beanUtils.copyProperties2SpuBoundTo(bounds);
        boundTo.setSpuId(infoEntity.getId());
        R r = couponFeginService.saveSpuBouns(boundTo);
        if (r.getCode() != 0) {
            log.error("调用远程服务保存spu积分信息失败，res={}", r);
        }

        //6、保存当前spu对应的sku信息
        List<SkusDTO> skus = spuInfoDto.getSkus();
        if (!CollectionUtils.isEmpty(skus)) {
            skus.forEach(item -> {
                String defaultImg = "";
                for (ImagesDTO image : item.getImages()) {
                    if (image.getDefaultImg() == 1) {
                        defaultImg = image.getImgUrl();
                        break;
                    }
                }
                SkuInfoEntity skuInfoEntity = beanUtils.skuDtoToEntity(item, infoEntity);
                skuInfoEntity.setSkuDefaultImg(defaultImg);
                //6.1)、sku的基本信息;pms_sku_info
                skuInfoService.saveSkuInfo(skuInfoEntity);
                Long skuId = skuInfoEntity.getSkuId();

                List<SkuImagesEntity> skuImagesEntities = item.getImages().stream().map(img -> {
                    SkuImagesEntity imageEntity = new SkuImagesEntity();
                    imageEntity.setSkuId(skuId);
                    imageEntity.setDefaultImg(img.getDefaultImg());
                    imageEntity.setImgUrl(img.getImgUrl());
                    return imageEntity;
                }).filter(obj -> StringUtils.isNotBlank(obj.getImgUrl())).collect(Collectors.toList());
                //6.2)、sku图片信息;pms_sku_images
                skuImagesService.saveBatch(skuImagesEntities);
                //6.3)、sku的销售属性信息:pms_sku_sale_attr_value
                List<SkuSaleAttrValueEntity> skuSaleAttrValueEntityList = item.getAttr().stream().map(obj -> {
                    SkuSaleAttrValueEntity attrValueEntity = beanUtils.skuSaleAttrValueDto2Entity(obj);
                    attrValueEntity.setSkuId(skuId);
                    return attrValueEntity;
                }).collect(Collectors.toList());

                skuSaleAttrValueService.saveBatch(skuSaleAttrValueEntityList);
                //6.4)、sku优惠满减信息gulimall_sms->sms_sku_ladder;sms_sku_full_reduction;sms_member_price
                SkuReductionTo skuReductionTo = beanUtils.SpuDto2SkuReductionTo(item);
                skuReductionTo.setSkuId(skuId);
                if (skuReductionTo.getFullCount() > 0 || skuReductionTo.getFullPrice().compareTo(new BigDecimal("0")) == 1) {
                    R res = couponFeginService.saveSkuReduction(skuReductionTo);

                    if (res.getCode() != 0) {
                        log.error("调用远程服务保存spu积分信息失败，res={}", r);
                    }
                }


            });
        }


    }

    @Override
    public void saveBaseSpuInfo(SpuInfoEntity spuInfo) {
        this.baseMapper.insert(spuInfo);
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {

        LambdaQueryWrapper<SpuInfoEntity> queryWrapper = new LambdaQueryWrapper<SpuInfoEntity>();
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.and((obj) -> {
                obj.eq(SpuInfoEntity::getId, key).or().like(SpuInfoEntity::getSpuName, key);
            });
        }

        String status = (String) params.get("status");
        if (StringUtils.isNotBlank(status)) {
            queryWrapper.eq(SpuInfoEntity::getPublishStatus, status);
        }
        String brandId = (String) params.get("brandId");
        if (StringUtils.isNotBlank(brandId) && !ProductConstant.COMMON_ZERO.equalsIgnoreCase(brandId)) {
            queryWrapper.eq(SpuInfoEntity::getBrandId, brandId);
        }
        String catelogId = (String) params.get("catelogId");
        if (StringUtils.isNotBlank(catelogId) && !ProductConstant.COMMON_ZERO.equalsIgnoreCase(catelogId)) {
            queryWrapper.eq(SpuInfoEntity::getCatalogId, catelogId);
        }
        IPage<SpuInfoEntity> page = this.page(new Query<SpuInfoEntity>().getPage(params), queryWrapper);
        return new PageUtils(page);


    }

    @Override
    public void up(Long spuId) {

        //组装需要的数据
        //1、根据spuId查询出下面所有的Sku信息
        List<SkuInfoEntity> skuInfoEntities = skuInfoService.getSkusBySpuId(spuId);
        List<Long> skuIds = skuInfoEntities.stream().map(SkuInfoEntity::getSkuId).collect(Collectors.toList());
        // 查询当前sku的所有可以用来检索的规格属性
        List<ProductAttrValueEntity> baseAttrs = attrValueService.listBaseAttrForSpu(spuId);
        //所有属性id集合
        List<Long> attrIds = baseAttrs.stream().map(ProductAttrValueEntity::getAttrId).collect(Collectors.toList());
        List<Long> searchIds = attrService.selectSearchAttrs(attrIds);
        HashSet<Long> setIds = Sets.newHashSet(searchIds);
        List<SkuEsModel.Attrs> attrsList = baseAttrs.stream().filter(item -> setIds.contains(item.getAttrId()))
                .map(item -> {
                    SkuEsModel.Attrs attrs = beanUtils.productAttrValue2EsModelAttr(item);
                    return attrs;
                }).collect(Collectors.toList());

        //hasStock todo 远程调用 库存系统查询是否有库存
        Map<Long, Boolean> resMap = null;
        try {
            Result<List<SkuHasStockVO>> result = wareFeginService.getSkusHasStock(skuIds);
            resMap = result.getData().stream().collect(Collectors.toMap(SkuHasStockVO::getSkuId, SkuHasStockVO::getHasStock));
        } catch (Exception e) {
            log.error("远程查询库存信息失败，原因:{}", e);
        }

        Map<Long, Boolean> finalResMap = resMap;
        List<SkuEsModel> skuEsModelList = skuInfoEntities.stream().map(sku -> {
            SkuEsModel esModel = beanUtils.skuTransToSkuEsModel(sku);
            //没有查询到库存，默认库存有数据
            esModel.setHasStock(finalResMap.get(sku.getSkuId()) == null ? true : finalResMap.get(sku.getSkuId()));
            //todo 热度评分，刚上架给0；（或者置顶）,现实中热度评分可能比较负责，可操作性比较高。
            esModel.setHotScore(0L);
            // TODO 插叙品牌和分类名字
            BrandEntity brand = brandService.getById(esModel.getBrandId());
            esModel.setBrandName(brand.getName());
            esModel.setBrandImg(brand.getLogo());
            CategoryEntity category = categoryService.getById(esModel.getCatalogId());
            esModel.setCatalogName(category.getName());
            esModel.setAttrs(attrsList);
            return esModel;
        }).collect(Collectors.toList());
        //todo 数据发送给es进行保存
        R result = searchFgeinService.productStatueUp(skuEsModelList);

        if (result.getCode().equals(0)) {
            baseMapper.updateSpuStatus(spuId, ProductConstant.StatusEnum.SPU_UP.getCode());
        } else {
            //todo 调用失败 重复调用？接口幂等性；重试机制
        }


    }

}