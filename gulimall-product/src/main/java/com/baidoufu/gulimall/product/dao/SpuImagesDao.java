package com.baidoufu.gulimall.product.dao;

import com.baidoufu.gulimall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:17
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {
	
}
