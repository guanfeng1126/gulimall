/**
  * Copyright 2020 bejson.com 
  */
package com.baidoufu.gulimall.product.dto.spu;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Auto-generated: 2020-06-03 10:27:48
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Setter
@Getter
@ToString
public class BaseAttrsDTO implements Serializable {

    private static final long serialVersionUID = -7121925861096261990L;
    private Long attrId;
    private String attrValues;
    private Integer showDesc;


}