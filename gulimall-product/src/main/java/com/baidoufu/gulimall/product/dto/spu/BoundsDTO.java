/**
  * Copyright 2020 bejson.com 
  */
package com.baidoufu.gulimall.product.dto.spu;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Auto-generated: 2020-06-03 10:27:48
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Setter
@Getter
@ToString
public class BoundsDTO implements Serializable {

    private static final long serialVersionUID = 4131138084760247272L;
    /**
     * 购物积分
     */
    private BigDecimal buyBounds;
    /**
     * 成长积分
     */
    private BigDecimal growBounds;

}