package com.baidoufu.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.R;
import com.baidoufu.gulimall.product.dto.AttrDTO;
import com.baidoufu.gulimall.product.entity.ProductAttrValueEntity;
import com.baidoufu.gulimall.product.service.CategoryService;
import com.baidoufu.gulimall.product.service.ProductAttrValueService;
import com.baidoufu.gulimall.product.vo.AttrRespVO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baidoufu.gulimall.product.entity.AttrEntity;
import com.baidoufu.gulimall.product.service.AttrService;


/**
 * 商品属性
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
@RestController
@RequestMapping("product/attr")
@RequiredArgsConstructor
public class AttrController {

    private final AttrService attrService;

    private final ProductAttrValueService productAttrValueService;


    /**
     * 规格和销售书列表查询
     *
     * @param params
     * @param catelogId
     * @param attrType  属性分类 base 基本属性  sale 销售属性
     * @return
     */
    //@GetMapping(value = "/base/list/{catelogId}")
    @GetMapping(value = "/{attrType}/list/{catelogId}")
    public R listBaseAttr(@RequestParam Map<String, Object> params,
                          @PathVariable("catelogId") Long catelogId,
                          @PathVariable("attrType") String attrType) {
        PageUtils page = attrService.queryBaseAttrPage(params, catelogId, attrType);
        return R.ok().put("page", page);
    }

    /**
     * 获取spu规格
     *
     * @param spuId
     * @return
     */
    @GetMapping(value = "/base/listforspu/{spuId}")
    public R listBaseAttrForSpu(@PathVariable("spuId") Long spuId) {
        List<ProductAttrValueEntity> valueEntityList = productAttrValueService.listBaseAttrForSpu(spuId);
        return R.ok().put("data", valueEntityList);
    }


    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("product:attr:list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 根据id查询基本信息
     */
    @RequestMapping("/info/{attrId}")
    //@RequiresPermissions("product:attr:info")
    public R info(@PathVariable("attrId") Long attrId) {
        AttrRespVO attrInfo = attrService.getAttrInfo(attrId);

        return R.ok().put("attr", attrInfo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attr:save")
    public R save(@RequestBody AttrDTO attrDTO) {
        attrService.saveAttr(attrDTO);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attr:update")
    public R update(@RequestBody AttrDTO attrDTO) {
        attrService.updateAttrDetail(attrDTO);

        return R.ok();
    }


    /**
     * 修改spu的规格属性
     *
     * @param spuId
     * @param valueEntities
     * @return
     */
    @PostMapping("/update/{spuId}")
    public R updateSpuAttr( @PathVariable("spuId") Long spuId, @RequestBody List<ProductAttrValueEntity> valueEntities) {
        productAttrValueService.upateSpuAttr(spuId, valueEntities);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attr:delete")
    public R delete(@RequestBody Long[] attrIds) {
        attrService.removeByIds(Arrays.asList(attrIds));

        return R.ok();
    }

}
