package com.baidoufu.gulimall.product.util;

import com.baidoufu.common.to.es.SkuEsModel;
import com.baidoufu.common.to.product.spu.MemberPriceTo;
import com.baidoufu.common.to.product.spu.SkuReductionTo;
import com.baidoufu.common.to.product.spu.SpuBoundTo;
import com.baidoufu.gulimall.product.dto.AttrDTO;
import com.baidoufu.gulimall.product.dto.AttrGroupRelationDTO;
import com.baidoufu.gulimall.product.dto.spu.*;
import com.baidoufu.gulimall.product.entity.*;
import com.baidoufu.gulimall.product.vo.AttrGroupWithAttrsVO;
import com.baidoufu.gulimall.product.vo.AttrRespVO;
import com.baidoufu.gulimall.product.vo.BrandVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

/**
 * @className: MapStructBeanUtils
 * @description: 实体类，vo，dto，想互转换工具类
 * @author: guanfeng
 * @date: 2020/5/31 14:53
 * @version: V1.0.0
 **/
@Mapper(componentModel = "spring")
public interface MapStructBeanUtils {
    /**
     * dto转换成 AttrEntity
     *
     * @param attrDTO
     * @return AttrEntity
     */
    AttrEntity attrDTO2Entity(AttrDTO attrDTO);

    /**
     * attr 转换成vo
     *
     * @param arrtEntity
     * @return
     */
    AttrRespVO attrToVo(AttrEntity arrtEntity);

    /**
     * dto 转换成关联关系表实体类
     *
     * @param dto
     * @return
     */
    AttrAttrgroupRelationEntity dtoToAttrGroupRelationEntity(AttrGroupRelationDTO dto);

    /**
     * 品牌实体集合转换成vo的list
     *
     * @param brandEntityList
     * @return
     */
    List<BrandVO> brandsToVoList(List<BrandEntity> brandEntityList);

    /**
     * 品牌实体转换成vo
     *
     * @param entity
     * @return
     */
    @Mapping(source = "name", target = "brandName")
    BrandVO brandToVo(BrandEntity entity);

    /**
     * group分组转换成 group&属性vo类
     *
     * @param group
     * @return
     */
    AttrGroupWithAttrsVO attrGroupToVo(AttrGroupEntity group);

    /**
     * spuinfodto转换成实体类
     *
     * @param spuInfo
     * @return
     */
    SpuInfoEntity spuInfoDtoToEntity(SpuSaveDTO spuInfo);

    /**
     * dto等组装成实体类
     *
     * @param item
     * @param infoEntity
     * @return
     */
    @Mappings({
            @Mapping(source = "infoEntity.id", target = "spuId"),
            @Mapping(target = "saleCount", constant = "0L")
    })
    SkuInfoEntity skuDtoToEntity(SkusDTO item, SpuInfoEntity infoEntity);

    /**
     * skusaleAttrValue dto转实体类
     *
     * @param obj
     * @return
     */
    SkuSaleAttrValueEntity skuSaleAttrValueDto2Entity(AttrSpuDTO obj);

    /**
     * bound dto转换成to
     *
     * @param bounds
     * @return
     */
    SpuBoundTo copyProperties2SpuBoundTo(BoundsDTO bounds);

    /**
     * skudto 转换成skuTo
     *
     * @param item
     * @return
     */
    SkuReductionTo SpuDto2SkuReductionTo(SkusDTO item);

    /**
     * memeberPrice dto List 转换成 to  list
     *
     * @param memberPriceDTOs
     * @return
     */
    List<MemberPriceTo> memberPrciceDtoList2MemeberPriceTos(List<MemberPriceDTO> memberPriceDTOs);

    /**
     * dto 转to
     *
     * @param memberPriceDTO
     * @return
     */
    MemberPriceTo memberPrciceDto2MemeberPriceTo(MemberPriceDTO memberPriceDTO);

    /**
     * sku -> skuesModel
     *
     * @param sku
     * @return
     */
    @Mappings({
            @Mapping(target = "skuPrice", source = "price"),
            @Mapping(target = "skuImg", source = "skuDefaultImg")

    })
    SkuEsModel skuTransToSkuEsModel(SkuInfoEntity sku);

    /**
     * 属性转换
     *
     * @param item
     * @return
     */
    SkuEsModel.Attrs productAttrValue2EsModelAttr(ProductAttrValueEntity item);
}
