package com.baidoufu.gulimall.product.vo;

import lombok.*;

import java.io.Serializable;
import java.util.List;

/**
 * @className: Catelog2VO
 * @description:
 * @author: guanfeng
 * @date: 2020/6/18 17:08
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Catelog2VO implements Serializable {

    private static final long serialVersionUID = -7824812108426492490L;
    private  String catalog1Id;

    private List<Catelog3VO> catalog3List;

    private String id;

    private String name;


}
