package com.baidoufu.gulimall.product.vo;

import lombok.*;

import java.io.Serializable;

/**
 * @className: Catelog3VO
 * @description:
 * @author: guanfeng
 * @date: 2020/6/18 17:10
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Catelog3VO implements Serializable {
    private static final long serialVersionUID = 3860208085114245951L;

    private String catalog2Id;
    private String name;

    private String id;
}
