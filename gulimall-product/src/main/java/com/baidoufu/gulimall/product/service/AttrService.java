package com.baidoufu.gulimall.product.service;

import com.baidoufu.gulimall.product.dto.AttrDTO;
import com.baidoufu.gulimall.product.entity.ProductAttrValueEntity;
import com.baidoufu.gulimall.product.vo.AttrRespVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存属性
     *
     * @param attrDTO
     */
    void saveAttr(AttrDTO attrDTO);

    /**
     * 根据分类id查询下面挂载的所有基本属性信息
     *
     * @param params
     * @param catelogId
     * @param attrType
     * @return
     */
    PageUtils queryBaseAttrPage(Map<String, Object> params, Long catelogId, String attrType);

    /**
     * 根据attrId查询属性的完整信息
     *
     * @param attrId
     * @return
     */
    AttrRespVO getAttrInfo(Long attrId);

    /**
     * 修改属性详细信息
     *
     * @param attrDTO
     */
    void updateAttrDetail(AttrDTO attrDTO);

    /**
     * 在指定集合查询可以被搜索的属性id集合
     * @param attrIds
     * @return
     */
    List<Long> selectSearchAttrs(List<Long> attrIds);
}

