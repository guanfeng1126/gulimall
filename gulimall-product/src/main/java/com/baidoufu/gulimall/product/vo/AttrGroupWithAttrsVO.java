package com.baidoufu.gulimall.product.vo;

import com.baidoufu.gulimall.product.entity.AttrEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @className: AttrGroupWithAttrsVO
 * @description: 分组信息和关联属性信息
 * @author: guanfeng
 * @date: 2020/6/3 8:48
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
public class AttrGroupWithAttrsVO implements Serializable {

    private static final long serialVersionUID = 319208397819300214L;
    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;
    /**
     * 分组关联的属性集合
     */
    private List<AttrEntity> attrs;
}
