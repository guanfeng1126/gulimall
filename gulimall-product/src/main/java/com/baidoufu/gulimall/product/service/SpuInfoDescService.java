package com.baidoufu.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:17
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存spu的 描述图片地址
     * @param descEntity
     */
    void saveSpuInfoDesc(SpuInfoDescEntity descEntity);
}

