package com.baidoufu.gulimall.product.fegin;

import com.baidoufu.common.utils.R;
import com.baidoufu.common.utils.Result;
import com.baidoufu.gulimall.product.vo.SkuHasStockVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @className: WareFeginService
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 16:10
 * @version: V1.0.0
 **/
@FeignClient("gulimall-ware")
public interface WareFeginService {

    /**
     * 查询sku中的库存量
     *
     * @param skuIds
     * @return
     */
    @PostMapping("/ware/waresku/hasstock")
    Result<List<SkuHasStockVO>> getSkusHasStock(@RequestBody List<Long> skuIds);
}
