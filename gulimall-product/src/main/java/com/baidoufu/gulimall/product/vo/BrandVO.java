package com.baidoufu.gulimall.product.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @className: BrandVO
 * @description:
 * @author: guanfeng
 * @date: 2020/6/3 0:21
 * @version: V1.0.0
 **/
@Getter
@Setter
@ToString
public class BrandVO implements Serializable {
    private static final long serialVersionUID = 830247509083407127L;

    private Long brandId;

    private String brandName;
}
