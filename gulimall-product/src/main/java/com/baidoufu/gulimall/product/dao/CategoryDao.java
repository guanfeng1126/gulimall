package com.baidoufu.gulimall.product.dao;

import com.baidoufu.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
