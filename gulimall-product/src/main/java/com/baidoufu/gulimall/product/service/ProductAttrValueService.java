package com.baidoufu.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:17
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存spu的属性值
     *
     * @param attrValueEntities
     */
    void saveProductAttr(List<ProductAttrValueEntity> attrValueEntities);

    /**
     * 查询spu的规格属性
     *
     * @param spuId
     * @return
     */
    List<ProductAttrValueEntity> listBaseAttrForSpu(Long spuId);

    /**
     * 更新spu相关规格
     *
     * @param spuId
     * @param valueEntities
     */
    void upateSpuAttr(Long spuId, List<ProductAttrValueEntity> valueEntities);
}

