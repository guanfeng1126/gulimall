package com.baidoufu.gulimall.product.service;

import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.dto.AttrGroupRelationDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.gulimall.product.entity.AttrAttrgroupRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 新增分组合属性关联关系
     * @param relationDTOS
     */
    void addRelationAttr(List<AttrGroupRelationDTO> relationDTOS);
}

