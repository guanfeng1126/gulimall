package com.baidoufu.gulimall.product.entity;

import com.baidoufu.common.valid.AddGroup;
import com.baidoufu.common.valid.ListValue;
import com.baidoufu.common.valid.UpdateGroup;
import com.baidoufu.common.valid.UpdateStatusGroup;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
    @Null(message = "新增品牌id不能指定",groups = {AddGroup.class})
    @NotNull(message = "修改品牌id不能为空",groups = {UpdateGroup.class})
	private Long brandId;
	/**
	 * 品牌名
	 */
    @NotBlank(message = "品牌名不能为空",groups = {AddGroup.class,UpdateGroup.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
	@URL(message = "logo必须是一个合法额URL地址",groups = {AddGroup.class,UpdateGroup.class})
    @NotBlank(groups = {AddGroup.class},message = "logo地址不能为空")
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@ListValue(value = {0,1},message = "显示状态只能在0-1之间",groups = {AddGroup.class, UpdateStatusGroup.class})
    @NotNull(groups = {AddGroup.class,UpdateStatusGroup.class},message = "显示状态不能为空")
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotBlank(groups = {AddGroup.class})
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索字母必须是一个字母",groups = {AddGroup.class,UpdateGroup.class})
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotEmpty
	@Min(value = 0,message = "排序必须大于等于0")
	private Integer sort;

}
