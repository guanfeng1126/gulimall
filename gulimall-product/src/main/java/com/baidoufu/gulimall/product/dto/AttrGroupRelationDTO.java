package com.baidoufu.gulimall.product.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @className: AttrGroupRelationDTO
 * @description: 属性和属性分组关联dto类
 * @author: guanfeng
 * @date: 2020/6/1 9:06
 * @version: V1.0.0
 **/
@Getter
@Setter
@ToString
public class AttrGroupRelationDTO implements Serializable {
    private static final long serialVersionUID = -8962919200764857253L;

    private Long attrId;

    private Long attrGroupId;
}
