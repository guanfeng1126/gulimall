/**
  * Copyright 2020 bejson.com 
  */
package com.baidoufu.gulimall.product.dto.spu;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Auto-generated: 2020-06-03 10:27:48
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Setter
@Getter
@ToString
public class SkusDTO implements Serializable {

    private static final long serialVersionUID = -8419970176638442377L;
    private List<AttrSpuDTO> attr;
    private String skuName;
    private BigDecimal price;
    private String skuTitle;
    private String skuSubtitle;
    private List<ImagesDTO> images;
    private List<String> descar;
    private Integer fullCount;
    private BigDecimal discount;
    /**
     * 是否叠加其他优惠[0-不可叠加，1-可叠加]
     */
    private Integer countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private Integer priceStatus;
    /**
     * 会员价格
     */
    private List<MemberPriceDTO> memberPrice;

}