package com.baidoufu.gulimall.product.fegin;

import com.baidoufu.common.to.es.SkuEsModel;
import com.baidoufu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @className: SearchFgeinService
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 17:39
 * @version: V1.0.0
 **/
@FeignClient("gulimall-search")
public interface SearchFgeinService {
    /**
     * 保存上架商品信息到es
     *
     * @param skuEsModelList
     * @return
     */
    @PostMapping("/search/save/product")
    public R productStatueUp(@RequestBody List<SkuEsModel> skuEsModelList);
}
