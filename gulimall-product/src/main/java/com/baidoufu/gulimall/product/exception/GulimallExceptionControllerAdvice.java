package com.baidoufu.gulimall.product.exception;

import com.baidoufu.common.exception.BizCodeEnum;
import com.baidoufu.common.exception.RRException;
import com.baidoufu.common.utils.R;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * @className: GulimallExceptionControllerAdvice
 * @description:
 * @author: guanfeng
 * @date: 2020/5/19 15:59
 * @version: V1.0.0
 **/
@RestControllerAdvice
@Slf4j
public class GulimallExceptionControllerAdvice {
    /**
     * 拦截数据校验
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleValidException(MethodArgumentNotValidException e) {
        log.error("数据校验出现问题{}，异常类型{}", e.getMessage(), e.getClass());
        BindingResult result = e.getBindingResult();
        Map<String, String> errorMap = Maps.newHashMap();
        result.getFieldErrors().forEach((item) -> {
            errorMap.put(item.getField(), item.getDefaultMessage());
        });
        return R.error(BizCodeEnum.VALID_EXCEPTION.getCode(), BizCodeEnum.VALID_EXCEPTION.getMsg()).put("data", errorMap);
    }

    @ExceptionHandler(value = RRException.class)
    public R handleGeneralException(RRException e) {
        log.error("出现通用异常，异常问题{}，异常类型{}", e.getMessage(), e.getClass());

        return R.error(e.getCode(), e.getMsg());
    }
}

