/**
  * Copyright 2020 bejson.com 
  */
package com.baidoufu.gulimall.product.dto.spu;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * Auto-generated: 2020-06-03 10:27:48
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Setter
@Getter
@ToString
public class SpuSaveDTO implements Serializable {

    private static final long serialVersionUID = -8235097546543503919L;
    private String spuName;
    private String spuDescription;
    private Long catalogId;
    private Long brandId;
    private Double weight;
    private Long  publishStatus;
    private List<String> decript;
    private List<String> images;
    private BoundsDTO bounds;
    private List<BaseAttrsDTO> baseAttrs;
    private List<SkusDTO> skus;

}