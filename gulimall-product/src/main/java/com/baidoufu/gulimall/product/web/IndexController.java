package com.baidoufu.gulimall.product.web;

import com.baidoufu.gulimall.product.entity.CategoryEntity;
import com.baidoufu.gulimall.product.service.CategoryService;
import com.baidoufu.gulimall.product.vo.Catelog2VO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * @className: IndexController
 * @description: 访问html controller
 * @author: guanfeng
 * @date: 2020/6/18 9:49
 * @version: V1.0.0
 **/
@Controller
@Slf4j
@RequiredArgsConstructor
public class IndexController {
    private final CategoryService categoryService;

    @GetMapping(value = {"/", "index.html"})
    public String indexPage(Model model) {
        //查询出一级分类
        List<CategoryEntity> categoryEntityList = categoryService.getLevel1Category();
        model.addAttribute("categorys", categoryEntityList);
        return "index";
    }

    @ResponseBody
    @GetMapping(value = "/index/catalog.json")
    public Map getCatalogJson() {
        Map<String, List<Catelog2VO>> map = categoryService.getCatalogJson();
        return map;
    }

    @ResponseBody
    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }
}
