package com.baidoufu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baidoufu.gulimall.product.service.CategoryBrandRelationService;
import com.baidoufu.gulimall.product.vo.Catelog2VO;
import com.baidoufu.gulimall.product.vo.Catelog3VO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;
import com.baidoufu.gulimall.product.dao.CategoryDao;
import com.baidoufu.gulimall.product.entity.CategoryEntity;
import com.baidoufu.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import static com.baidoufu.common.constant.ProductConstant.CATEGORY_JSON_CACHE;


/**
 * @author baidoufu
 */
@Service("categoryService")
@Slf4j
@RequiredArgsConstructor
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    private final CategoryBrandRelationService categoryBrandRelationService;

    private final StringRedisTemplate redisTemplate;

    private final RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1、查出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);
        //2、组装父子结构
        List<CategoryEntity> menus = entities.stream().filter(categoryEntity -> categoryEntity.getParentCid() == 0)
                .map(menu -> {
                    menu.setChildren(getCategoryChildren(menu, entities));
                    return menu;
                })
                .sorted(Comparator.comparingInt(men -> (men.getSort() == null ? 0 : men.getSort())))
                .collect(Collectors.toList());
        return menus;
    }

    @Override
    public void removeMenusByIds(List<Long> catIds) {
        //todo 后期增加删除逻辑
        baseMapper.deleteBatchIds(catIds);
    }

    @Override
    public Long[] findCateLogPath(Long categoryId) {
        List<Long> paths = Lists.newArrayList();
        findParentPath(categoryId, paths);
        Collections.reverse(paths);
        return paths.toArray(new Long[paths.size()]);
    }


//    @Caching(evict = {
//            @CacheEvict(value = {"category"},key = "'getLevel1Category'"),
//            @CacheEvict(value = {"category"},key = "'getCatalogJson'")
//    })
    @CacheEvict(value = {"category"},allEntries = true)//删除category分区所有的key
    @Override
    @Transactional
    public void updateCascade(CategoryEntity category) {
        this.updateById(category);
        //更新category之后需要吧缓存数据删除
        //redisTemplate.delete(CATEGORY_JSON_CACHE);
        //修改了分类名字，其他表冗余字段也要修改
        if (StringUtils.isNotBlank(category.getName())) {
            categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());
            //todo 其他表字段
        }

    }

    @Override
    @Cacheable(value = "category",key="#root.method.name",sync = true)//使用cache缓存
    public List<CategoryEntity> getLevel1Category() {

        List<CategoryEntity> categoryEntityList = baseMapper.selectList(
                new LambdaQueryWrapper<CategoryEntity>().eq(CategoryEntity::getParentCid, 0));

        return categoryEntityList;


    }

    @Override
    @Cacheable(value = {"category"},key="#root.method.name")
    public Map<String, List<Catelog2VO>> getCatalogJson() {
        log.info("查询数据库。。。。");
        //查询出所有，一次查询
        List<CategoryEntity> selectList = baseMapper.selectList(null);

        //查询所有的一级分类
        List<CategoryEntity> level1CategoryList = getParentCid(selectList, 0L);
        Map<String, List<Catelog2VO>> listMap = level1CategoryList.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //v=一级分类，查询所有的二级分类
            List<CategoryEntity> level2CategoryList = getParentCid(selectList, v.getCatId());
            List<Catelog2VO> catelog2VOList = null;
            if (!CollectionUtils.isEmpty(level2CategoryList)) {
                //二级分类数据分装
                catelog2VOList = level2CategoryList.stream().map(item -> {
                    Catelog2VO catelog2VO = new Catelog2VO(v.getCatId().toString(), null, item.getCatId().toString(), item.getName());
                    //查询三级分类
                    List<CategoryEntity> level3CategoryList = getParentCid(selectList, item.getCatId());
                    //三级分类封装
                    if (!CollectionUtils.isEmpty(level3CategoryList)) {
                        List<Catelog3VO> catelog3VOList = level3CategoryList.stream().map(level3Category -> {
                            Catelog3VO catelog3VO = new Catelog3VO(item.getCatId().toString(), level3Category.getName(), level3Category.getCatId().toString());
                            return catelog3VO;
                        }).collect(Collectors.toList());
                        catelog2VO.setCatalog3List(catelog3VOList);
                    }
                    return catelog2VO;
                }).collect(Collectors.toList());
            }
            return catelog2VOList;
        }));
        //放入缓存
        //String json = JSON.toJSONString(listMap);
        //加过期时间
        //redisTemplate.opsForValue().set(CATEGORY_JSON_CACHE, json, 1, TimeUnit.DAYS);
        return listMap;
    }

    /**
     * 优化，直接走缓存
     *
     * @return
     */
   // @Override
    public Map<String, List<Catelog2VO>> getCatalogJson2() {
        //首先查询缓存中是否存在，
        /**
         * 1、空结果缓存：解决缓存穿透
         * 2、设置过期时间（加随机值），解决缓存雪崩
         * 3、加锁：解决缓存击穿
         */
        String catalogJson = redisTemplate.opsForValue().get(CATEGORY_JSON_CACHE);

        if (StringUtils.isBlank(catalogJson)) {
            log.info("缓存不命中...查询数据库...");
            Map<String, List<Catelog2VO>> catalogJsonFromDb = getCatalogJsonFromDbWithRedisLock();
            return catalogJsonFromDb;
        }
        log.info("缓存命中，直接返回数据......");
        Map<String, List<Catelog2VO>> result = JSON.parseObject(catalogJson, new TypeReference<Map<String, List<Catelog2VO>>>() {
        });

        return result;

    }

    /**
     * 使用redisson进行分布式锁
     *
     * @return
     */
    public Map<String, List<Catelog2VO>> getCatalogJsonFromDbWithRedissonLock() {
        //1.锁的名字。锁的粒度，越细越好
        //锁的粒度：具体缓存的是某个数据，11-号商品：product-11-lock product-12-lock
        RLock lock = redisson.getLock("CatalogJson-lock");
        lock.lock();

        log.info("获取分布式锁成功... ");
        Map<String, List<Catelog2VO>> dataFromDb;
        try {
            dataFromDb = getCategoryDataFromDb();
        } finally {
            lock.unlock();
        }
        return dataFromDb;

    }


    /**
     * 优化：
     * 1、多次查询数据库变成1次
     * 查询数据库
     *
     * @return
     */
    public Map<String, List<Catelog2VO>> getCatalogJsonFromDbWithRedisLock() {
        String token = UUID.randomUUID().toString();
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("lock", token, 300, TimeUnit.SECONDS);
        if (lock) {
            log.info("获取分布式锁成功... ");
            //获取锁 执行业务
            //设置过期时间,设置过期时间要原子操作
            //redisTemplate.expire("lock",30,TimeUnit.SECONDS);
            Map<String, List<Catelog2VO>> dataFromDb;
            try {
                dataFromDb = getCategoryDataFromDb();
            } finally {
                //获取值+对比值=原子操作 使用lua脚本,不管结果如何最终都要删除锁
                String script = "if redis.call('get',KEYS[1]) == ARGV[1] then return redis.call('del',KEYS[1]) else return 0 end";
                Long lock1 = redisTemplate.execute(new DefaultRedisScript<Long>(script, Long.class), Arrays.asList("lock"), token);
            }
//            String lockValue = redisTemplate.opsForValue().get("lock");
//            if(token.equalsIgnoreCase(lockValue)) {
//                //删除锁
//                redisTemplate.delete("lock");
//            }
            return dataFromDb;
        } else {
            //加锁失败...重试
            log.error("获取分布式锁失败，等待重试.....");
            //睡醒200ms再次重试
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return getCatalogJsonFromDbWithRedisLock();
        }
    }

    /**
     * 从数据库中拿到category数据
     *
     * @return
     */
    private Map<String, List<Catelog2VO>> getCategoryDataFromDb() {
        //拿到锁，在去缓存中判断一下
        String catalogJson = redisTemplate.opsForValue().get(CATEGORY_JSON_CACHE);
        //存在即 直接返回不需要查库
        if (StringUtils.isNotBlank(catalogJson)) {
            Map<String, List<Catelog2VO>> result = JSON.parseObject(catalogJson, new TypeReference<Map<String, List<Catelog2VO>>>() {
            });

            return result;
        }

        log.info("查询了数据库。。。");

        //查询出所有，一次查询
        List<CategoryEntity> selectList = baseMapper.selectList(null);

        //查询所有的一级分类
        List<CategoryEntity> level1CategoryList = getParentCid(selectList, 0L);
        Map<String, List<Catelog2VO>> listMap = level1CategoryList.stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //v=一级分类，查询所有的二级分类
            List<CategoryEntity> level2CategoryList = getParentCid(selectList, v.getCatId());
            List<Catelog2VO> catelog2VOList = null;
            if (!CollectionUtils.isEmpty(level2CategoryList)) {
                //二级分类数据分装
                catelog2VOList = level2CategoryList.stream().map(item -> {
                    Catelog2VO catelog2VO = new Catelog2VO(v.getCatId().toString(), null, item.getCatId().toString(), item.getName());
                    //查询三级分类
                    List<CategoryEntity> level3CategoryList = getParentCid(selectList, item.getCatId());
                    //三级分类封装
                    if (!CollectionUtils.isEmpty(level3CategoryList)) {
                        List<Catelog3VO> catelog3VOList = level3CategoryList.stream().map(level3Category -> {
                            Catelog3VO catelog3VO = new Catelog3VO(item.getCatId().toString(), level3Category.getName(), level3Category.getCatId().toString());
                            return catelog3VO;
                        }).collect(Collectors.toList());
                        catelog2VO.setCatalog3List(catelog3VOList);
                    }
                    return catelog2VO;
                }).collect(Collectors.toList());
            }
            return catelog2VOList;
        }));
        //放入缓存
        String json = JSON.toJSONString(listMap);
        //加过期时间
        redisTemplate.opsForValue().set(CATEGORY_JSON_CACHE, json, 1, TimeUnit.DAYS);
        return listMap;
    }


    public Map<String, List<Catelog2VO>> getCatalogJsonFromDbWithLocalLock() {
        //TODO 本地锁，synchronized，juc（lock），在分布式情况下，想要锁住所有，需要使用分布式锁
        synchronized (this) {
            //拿到锁，在去缓存中判断一下
            return getCategoryDataFromDb();
        }
        // return null;
    }

    /**
     * 根据 pcid 判断子分类
     *
     * @param selectList
     * @param pCid
     * @return
     */
    private List<CategoryEntity> getParentCid(List<CategoryEntity> selectList, Long pCid) {
        List<CategoryEntity> collect = selectList.stream().filter(item -> pCid.equals(item.getParentCid())).collect(Collectors.toList());
        return collect;
    }

    private void findParentPath(Long categoryId, List<Long> paths) {
        paths.add(categoryId);
        CategoryEntity categoryEntity = this.getById(categoryId);
        //父id不等于0，继续遍历
        if (categoryEntity.getParentCid() != 0) {
            findParentPath(categoryEntity.getParentCid(), paths);
        }
    }


    /**
     * 递归查询所有菜单的子菜单
     *
     * @param root
     * @param all
     * @return
     */
    private List<CategoryEntity> getCategoryChildren(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> children = all.stream()
                .filter(categoryEntity -> root.getCatId().equals(categoryEntity.getParentCid()))
                .map(menu -> {
                    menu.setChildren(getCategoryChildren(menu, all));
                    return menu;
                }).sorted(Comparator.comparingInt(men -> (men.getSort() == null ? 0 : men.getSort())))
                .collect(Collectors.toList());

        return children;
    }

}