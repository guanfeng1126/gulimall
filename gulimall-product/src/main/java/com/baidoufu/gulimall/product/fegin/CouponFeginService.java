package com.baidoufu.gulimall.product.fegin;

import com.baidoufu.common.to.product.spu.SkuReductionTo;
import com.baidoufu.common.to.product.spu.SpuBoundTo;
import com.baidoufu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @className: CouponFeginService
 * @description:
 * @author: guanfeng
 * @date: 2020/6/3 23:05
 * @version: V1.0.0
 **/
@FeignClient("gulimall-coupon")
public interface CouponFeginService {
    /**
     * 保存spu的购物积分规则
     * @param boundTo
     * @return
     */
    @PostMapping(value = "/coupon/spubounds/save")
    R saveSpuBouns(@RequestBody SpuBoundTo boundTo);

    /**
     * 保存sku的折扣信息
     * @param skuReductionTo
     * @return
     */
    @PostMapping(value = "/coupon/skufullreduction/saveInfo")
    R saveSkuReduction(@RequestBody SkuReductionTo skuReductionTo);
}
