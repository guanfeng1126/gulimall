package com.baidoufu.gulimall.product.service.impl;

import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;
import com.baidoufu.gulimall.product.dao.BrandDao;
import com.baidoufu.gulimall.product.entity.BrandEntity;
import com.baidoufu.gulimall.product.service.BrandService;
import com.baidoufu.gulimall.product.service.CategoryBrandRelationService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;
import java.util.Map;


@Service("brandService")
@Slf4j
@RequiredArgsConstructor
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    private final BrandDao brandDao;

    private final CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        LambdaQueryWrapper<BrandEntity> queryWrapper = new LambdaQueryWrapper<BrandEntity>().
                eq(BrandEntity::getBrandId, key).or().like(BrandEntity::getName, key);
        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int saveBathTest(List<BrandEntity> brandEntityList) {
        int res = 0;
        List<Object> objects = Lists.newArrayList();
        for (BrandEntity brand : brandEntityList) {
            testRollBack(brand, objects);
        }

        log.info("插入失败的条数为{}，数据是={}", objects.size(), objects);
        return res;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDetail(BrandEntity brand) {
        this.updateById(brand);
        if (StringUtils.isNotBlank(brand.getName())) {
            //同步更新其他关联表
            categoryBrandRelationService.updateBrand(brand.getBrandId(),brand.getName());
            //todo 更新其他关联表
        }
    }

    //@Transactional(rollbackFor = Exception.class,propagation = Propagation.REQUIRES_NEW)
    public void testRollBack(BrandEntity brandEntity, List<Object> objects) {
        Object savePoint = TransactionAspectSupport.currentTransactionStatus().createSavepoint();
        try {
            testAdd(brandEntity);
        } catch (Exception e) {
            objects.add(brandEntity);
            log.error("插入品牌失败，brand={}", brandEntity);
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().rollbackToSavepoint(savePoint);
        }
    }


    private void testAdd(BrandEntity brandEntity) {
        brandDao.insert(brandEntity);
        if (brandEntity.getName().equals("test")) {
            QueryWrapper<BrandEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("name", "test");
            BrandEntity dbRes = brandDao.selectOne(queryWrapper);
            log.info("数据库里面的新增数据：{}", dbRes);
            dbRes.setName("test第一次更新");
            brandDao.updateById(dbRes);
        }
        brandEntity.setName(brandEntity.getName() + "第二次插入");
        brandDao.insert(brandEntity);
    }

}