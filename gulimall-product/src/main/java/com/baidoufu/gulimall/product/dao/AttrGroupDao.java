package com.baidoufu.gulimall.product.dao;

import com.baidoufu.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
