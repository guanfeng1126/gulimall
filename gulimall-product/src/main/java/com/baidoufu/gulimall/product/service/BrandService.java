package com.baidoufu.gulimall.product.service;

import com.baidoufu.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.gulimall.product.entity.BrandEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
public interface BrandService extends IService<BrandEntity> {

    PageUtils queryPage(Map<String, Object> params);

    int saveBathTest(List<BrandEntity> brandEntityList);

    /**
     * 更新brand 包括其它表的冗余字段
     * @param brand
     */
    void updateDetail(BrandEntity brand);
}

