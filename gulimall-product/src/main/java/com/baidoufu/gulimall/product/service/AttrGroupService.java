package com.baidoufu.gulimall.product.service;

import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.product.dto.AttrGroupRelationDTO;
import com.baidoufu.gulimall.product.entity.AttrEntity;
import com.baidoufu.gulimall.product.vo.AttrGroupWithAttrsVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.gulimall.product.entity.AttrGroupEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId);

    /**
     * 根据属性分组id获取关联的属性列表
     * @param attrgroupId
     * @return
     */
    List<AttrEntity> getAttrRelationList(Long attrgroupId);

    /**
     * 批量删除
     * @param relationDTOS
     */
    void deleteBatchRelation(List<AttrGroupRelationDTO> relationDTOS);

    /**
     * 查找分组所在的分类下面没有被关联的属性列表
     * @param attrgroupId
     * @param params
     * @return
     */
    PageUtils listNoRelationAttr(Long attrgroupId, Map<String, Object> params);

    /**
     * 查询指定分类下的分组信息&属性信息
     * @param catelogId
     * @return
     */
    List<AttrGroupWithAttrsVO> getAttrGroupWithAttrsByCatelogId(Long catelogId);
}

