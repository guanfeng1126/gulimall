package com.baidoufu.gulimall.product.service.impl;

import com.baidoufu.common.constant.ProductConstant;
import com.baidoufu.gulimall.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.product.dao.SkuInfoDao;
import com.baidoufu.gulimall.product.entity.SkuInfoEntity;
import com.baidoufu.gulimall.product.service.SkuInfoService;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveSkuInfo(SkuInfoEntity skuInfoEntity) {
        this.baseMapper.insert(skuInfoEntity);
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        LambdaQueryWrapper<SkuInfoEntity> queryWrapper = new LambdaQueryWrapper<SkuInfoEntity>();
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.and((obj) -> {
                obj.eq(SkuInfoEntity::getSkuId, key).or().like(SkuInfoEntity::getSkuName, key);
            });
        }

//        String status = (String) params.get("status");
//        if (StringUtils.isNotBlank(status)) {
//            queryWrapper.eq(SkuInfoEntity::getPublishStatus, status);
//        }
        String brandId = (String) params.get("brandId");
        if (StringUtils.isNotBlank(brandId) && !ProductConstant.COMMON_ZERO.equalsIgnoreCase(brandId)) {
            queryWrapper.eq(SkuInfoEntity::getBrandId, brandId);
        }
        String catelogId = (String) params.get("catelogId");
        if (StringUtils.isNotBlank(catelogId) && !ProductConstant.COMMON_ZERO.equalsIgnoreCase(catelogId)) {
            queryWrapper.eq(SkuInfoEntity::getCatalogId, catelogId);
        }

        String min = (String) params.get("min");
        if (StringUtils.isNotBlank(min)) {
            queryWrapper.ge(SkuInfoEntity::getPrice, min);
        }
        String max = (String) params.get("max");
        if (StringUtils.isNotBlank(catelogId)) {
            try {
                BigDecimal maxBigDecimal = new BigDecimal(max);
                if ((maxBigDecimal.compareTo(new BigDecimal(0))) == 1) {
                    queryWrapper.le(SkuInfoEntity::getPrice, max);
                }

            } catch (Exception e) {
                e.printStackTrace();
                log.error("查询sku商品分页数据的时候bigDecimal转换失败,e={}", e);
            }
        }


        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<SkuInfoEntity> getSkusBySpuId(Long spuId) {
        if (Objects.isNull(spuId)) {
            //todo 抛异常
        }

        LambdaQueryWrapper<SkuInfoEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SkuInfoEntity::getSpuId, spuId);
        List<SkuInfoEntity> skuInfoEntities = this.list(wrapper);
        return skuInfoEntities;
    }

}