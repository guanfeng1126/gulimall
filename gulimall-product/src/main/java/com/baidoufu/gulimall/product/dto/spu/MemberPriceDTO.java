/**
  * Copyright 2020 bejson.com 
  */
package com.baidoufu.gulimall.product.dto.spu;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

@Setter
@Getter
@ToString
public class MemberPriceDTO implements Serializable {

    private static final long serialVersionUID = -4517267502523728528L;
    private Long id;
    private String name;
    /**
     * 会员价格
     */
    private BigDecimal price;

}