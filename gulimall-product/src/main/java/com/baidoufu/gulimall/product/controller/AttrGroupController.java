package com.baidoufu.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.baidoufu.gulimall.product.dto.AttrGroupRelationDTO;
import com.baidoufu.gulimall.product.entity.AttrEntity;
import com.baidoufu.gulimall.product.service.AttrAttrgroupRelationService;
import com.baidoufu.gulimall.product.service.CategoryService;
import com.baidoufu.gulimall.product.vo.AttrGroupWithAttrsVO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.baidoufu.gulimall.product.entity.AttrGroupEntity;
import com.baidoufu.gulimall.product.service.AttrGroupService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.R;


/**
 * 属性分组
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-03 19:40:18
 */
@RestController
@RequestMapping("product/attrgroup")
@RequiredArgsConstructor
public class AttrGroupController {

    private final AttrGroupService attrGroupService;
    private final CategoryService categoryService;
    private final AttrAttrgroupRelationService relationService;

    /**
     * 查询属性分组关联的属性列表
     *
     * @param attrgroupId
     * @return
     */
    @GetMapping(value = "/{attrgroupId}/attr/relation")
    public R listAttrRelation(@PathVariable("attrgroupId") Long attrgroupId) {
        List<AttrEntity> attrEntityList = attrGroupService.getAttrRelationList(attrgroupId);
        return R.ok().put("data", attrEntityList);
    }

    @GetMapping(value = "/{catelogId}/withattr")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId")Long catelogId){
        List<AttrGroupWithAttrsVO>groupWithAttrsVOS =attrGroupService.getAttrGroupWithAttrsByCatelogId(catelogId);
      return   R.ok().put("data",groupWithAttrsVOS);
    }


    /**
     * 新增当前属性关联关系
     * @param relationDTOS
     * @return
     */
    @PostMapping(value = "/attr/relation")
    public R addRelationAttr(@RequestBody List<AttrGroupRelationDTO> relationDTOS) {
        relationService.addRelationAttr(relationDTOS);

        return R.ok();
    }

    /**
     * 查询当前分类下未被关联的属性分页数据
     * @param attrgroupId
     * @param params
     * @return
     */
    ///product/attrgroup/{attrgroupId}/noattr/relation
    @GetMapping(value = "/{attrgroupId}/noattr/relation")
    public R listNoRelationAttr(@PathVariable("attrgroupId") Long attrgroupId,
                                @RequestParam Map<String, Object> params) {
      PageUtils page = attrGroupService.listNoRelationAttr(attrgroupId,params);
        return R.ok().put("page", page);
    }

    @PostMapping(value = "/attr/relation/delete")
    public R deleteGroupRelation(@RequestBody List<AttrGroupRelationDTO> relationDTOS) {
        attrGroupService.deleteBatchRelation(relationDTOS);

        return R.ok();
    }

    /**
     * 列表
     */
    @GetMapping("/list/{catelogId}")
    //@RequiresPermissions("product:attrgroup:list")
    public R list(@RequestParam Map<String, Object> params, @PathVariable("catelogId") Long catelogId) {
//        PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params, catelogId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    //@RequiresPermissions("product:attrgroup:info")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {
        AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        Long categoryId = attrGroup.getCatelogId();
        //查找属性分组完整路径
        Long[] paths = categoryService.findCateLogPath(categoryId);
        attrGroup.setCatelogPath(paths);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("product:attrgroup:save")
    public R save(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("product:attrgroup:update")
    public R update(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("product:attrgroup:delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
        attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
