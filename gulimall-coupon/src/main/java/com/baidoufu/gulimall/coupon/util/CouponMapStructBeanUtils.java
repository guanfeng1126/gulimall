package com.baidoufu.gulimall.coupon.util;

import com.baidoufu.common.to.product.spu.MemberPriceTo;
import com.baidoufu.common.to.product.spu.SkuReductionTo;
import com.baidoufu.gulimall.coupon.entity.MemberPriceEntity;
import com.baidoufu.gulimall.coupon.entity.SkuFullReductionEntity;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @className: CouponMapStructBeanUtils
 * @description:
 * @author: guanfeng
 * @date: 2020/6/4 16:50
 * @version: V1.0.0
 **/
@Mapper(componentModel = "spring")
public interface CouponMapStructBeanUtils {
    /**
     * reduction to 转换成实体类
     *
     * @param reductionTo
     * @return
     */
    SkuFullReductionEntity skuReductionTo2SkuFullReduction(SkuReductionTo reductionTo);

    /**
     * to 2 entity
     * @param memberPrices
     * @return
     */
    List<MemberPriceEntity> memberPriceTo2MemberPriceEntity(List<MemberPriceTo> memberPrices);

    /**
     * to 2 entity
     *
     * @param memberPriceTo
     * @return
     */
    MemberPriceEntity memberPriceTo2MemberPrice(MemberPriceTo memberPriceTo);
}
