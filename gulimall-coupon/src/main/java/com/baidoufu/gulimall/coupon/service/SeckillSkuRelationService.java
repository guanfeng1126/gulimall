package com.baidoufu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.coupon.entity.SeckillSkuRelationEntity;

import java.util.Map;

/**
 * 秒杀活动商品关联
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 11:12:09
 */
public interface SeckillSkuRelationService extends IService<SeckillSkuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

