package com.baidoufu.gulimall.coupon.service;

import com.baidoufu.common.to.product.spu.SkuReductionTo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 11:12:09
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存sku的满减和折扣信息
     * @param reductionTo
     */
    void saveSkuReduction(SkuReductionTo reductionTo);
}

