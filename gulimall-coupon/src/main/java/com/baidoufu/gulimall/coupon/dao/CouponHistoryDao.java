package com.baidoufu.gulimall.coupon.dao;

import com.baidoufu.gulimall.coupon.entity.CouponHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券领取历史记录
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 11:12:10
 */
@Mapper
public interface CouponHistoryDao extends BaseMapper<CouponHistoryEntity> {
	
}
