package com.baidoufu.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 11:12:10
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

