package com.baidoufu.gulimall.coupon.dao;

import com.baidoufu.gulimall.coupon.entity.CouponSpuCategoryRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券分类关联
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 11:12:10
 */
@Mapper
public interface CouponSpuCategoryRelationDao extends BaseMapper<CouponSpuCategoryRelationEntity> {
	
}
