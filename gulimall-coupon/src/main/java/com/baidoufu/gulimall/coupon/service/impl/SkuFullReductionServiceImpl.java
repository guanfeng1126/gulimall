package com.baidoufu.gulimall.coupon.service.impl;

import com.baidoufu.common.to.product.spu.MemberPriceTo;
import com.baidoufu.common.to.product.spu.SkuReductionTo;
import com.baidoufu.gulimall.coupon.entity.MemberPriceEntity;
import com.baidoufu.gulimall.coupon.entity.SkuLadderEntity;
import com.baidoufu.gulimall.coupon.service.MemberPriceService;
import com.baidoufu.gulimall.coupon.service.SkuLadderService;
import com.baidoufu.gulimall.coupon.util.CouponMapStructBeanUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.coupon.dao.SkuFullReductionDao;
import com.baidoufu.gulimall.coupon.entity.SkuFullReductionEntity;
import com.baidoufu.gulimall.coupon.service.SkuFullReductionService;
import org.springframework.transaction.annotation.Transactional;


@Service("skuFullReductionService")
@RequiredArgsConstructor
@Slf4j
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    private final SkuLadderService skuLadderService;

    private final CouponMapStructBeanUtils beanUtils;

    private final MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<SkuFullReductionEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveSkuReduction(SkuReductionTo reductionTo) {
        //sku优惠满减信息gulimall_sms->sms_sku_ladder;sms_sku_full_reduction;sms_member_price
        //1、满减打折
        SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
        skuLadderEntity.setAddOther(reductionTo.getCountStatus());
        skuLadderEntity.setFullCount(reductionTo.getFullCount());
        skuLadderEntity.setDiscount(reductionTo.getDiscount());
        skuLadderEntity.setSkuId(reductionTo.getSkuId());
        //是否存在满减几件打折
        if (reductionTo.getFullCount() > 0) {
            skuLadderService.save(skuLadderEntity);
        }

        //2、保存sms_sku_full_reduction
        SkuFullReductionEntity skuFullReductionEntity = beanUtils.skuReductionTo2SkuFullReduction(reductionTo);
        skuFullReductionEntity.setSkuId(reductionTo.getSkuId());
        skuFullReductionEntity.setAddOther(reductionTo.getCountStatus());
        //是否存在满减多少元 打折
        if (reductionTo.getFullPrice().compareTo(new BigDecimal("0")) == 1) {
            this.save(skuFullReductionEntity);
        }

        //3、保存sms_member_price
        List<MemberPriceTo> memberPrices = reductionTo.getMemberPrice();
        List<MemberPriceEntity> memberPriceEntities = memberPrices.stream().
                filter(item -> (item.getPrice().compareTo(new BigDecimal("0")) == 1)).
                map(item -> {
                    MemberPriceEntity memberPriceEntity = new MemberPriceEntity();
                    memberPriceEntity.setAddOther(reductionTo.getCountStatus());
                    memberPriceEntity.setMemberLevelId(item.getId());
                    memberPriceEntity.setMemberLevelName(item.getName());
                    memberPriceEntity.setMemberPrice(item.getPrice());
                    return memberPriceEntity;
                }).collect(Collectors.toList());
        memberPriceService.saveBatch(memberPriceEntities);


    }

}