package com.baidoufu.gulimall.member.dao;

import com.baidoufu.gulimall.member.entity.IntegrationChangeHistoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 积分变化历史记录
 * 
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 14:22:41
 */
@Mapper
public interface IntegrationChangeHistoryDao extends BaseMapper<IntegrationChangeHistoryEntity> {
	
}
