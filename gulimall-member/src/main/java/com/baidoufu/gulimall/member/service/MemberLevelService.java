package com.baidoufu.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 14:22:40
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

