package com.baidoufu.gulimall.ware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @className: PurchaseDoneDTO
 * @description: 采购单完成dto
 * @author: guanfeng
 * @date: 2020/6/6 18:14
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
public class PurchaseDoneDTO implements Serializable {
    private static final long serialVersionUID = 4483753605098002194L;
    /**
     * 采购单id
     */
    private Long id;
    /**
     * 采购需求集合
     */
    private List<PurchaseItemDoneDTO> items;
}
