package com.baidoufu.gulimall.ware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @className: PurchaseMergeDTO
 * @description: 采购需求合并dto
 * @author: guanfeng
 * @date: 2020/6/6 15:15
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
public class PurchaseMergeDTO implements Serializable {
    private static final long serialVersionUID = 5622411667867232737L;
    /**
     * 采购单id
     */
    private Long  purchaseId;
    /**
     * 采购需求id集合
     */
    private List<Long> items;
}
