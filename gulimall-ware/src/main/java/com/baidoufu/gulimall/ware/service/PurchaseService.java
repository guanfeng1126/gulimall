package com.baidoufu.gulimall.ware.service;

import com.baidoufu.gulimall.ware.dto.PurchaseDoneDTO;
import com.baidoufu.gulimall.ware.dto.PurchaseMergeDTO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.ware.entity.PurchaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 14:42:41
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询未被领取的采购单
     *
     * @param params
     * @return
     */
    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);

    /**
     * 合并采购需求到指定采购单
     *
     * @param mergeDTO
     */
    void mergePurchase(PurchaseMergeDTO mergeDTO);

    /**
     * 领取采购单
     * @param ids
     */
    void received(List<Long> ids);

    /**
     * 完成采购单
     * @param doneDTO
     */
    void done(PurchaseDoneDTO doneDTO);
}

