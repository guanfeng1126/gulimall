package com.baidoufu.gulimall.ware.dao;

import com.baidoufu.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 商品库存
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 14:42:40
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
    /**
     * 增加库存
     *
     * @param skuId
     * @param wareId
     * @param skuNum
     */
    Integer addStock(@Param("skuId") Long skuId, @Param("wareId") Long wareId, @Param("skuNum") Integer skuNum);

    /**
     * 查询sku的库存量
     * @param skuId
     * @return
     */
    Long getSkuStock(@Param("skuId") Long skuId);
}
