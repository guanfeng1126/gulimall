package com.baidoufu.gulimall.ware.service;

import com.baidoufu.gulimall.ware.vo.SkuHasStockVO;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.gulimall.ware.entity.WareSkuEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author baidoufu
 * @email guanfeng.baidoufu@gmail.com
 * @date 2020-04-04 14:42:40
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 增加商品库存
     *
     * @param skuId  skuid
     * @param wareId 仓库id
     * @param skuNum 入库数量
     */
    void addStock(Long skuId, Long wareId, Integer skuNum);

    /**
     * 检查sku 库存
     * @param skuIds
     * @return
     */
    List<SkuHasStockVO> getSkusHasStock(List<Long> skuIds);
}

