package com.baidoufu.gulimall.ware.service.impl;

import com.baidoufu.common.utils.R;
import com.baidoufu.gulimall.ware.fegin.ProductFeginService;
import com.baidoufu.gulimall.ware.vo.SkuHasStockVO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.ware.dao.WareSkuDao;
import com.baidoufu.gulimall.ware.entity.WareSkuEntity;
import com.baidoufu.gulimall.ware.service.WareSkuService;


@Service("wareSkuService")
@Slf4j
@RequiredArgsConstructor
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {
    private final WareSkuDao wareSkuDao;

    private final ProductFeginService productFeginService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        LambdaQueryWrapper<WareSkuEntity> queryWrapper = new LambdaQueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if (StringUtils.isNotBlank(skuId)) {
            queryWrapper.eq(WareSkuEntity::getSkuId, skuId);
        }
        String wareId = (String) params.get("wareId");
        if (StringUtils.isNotBlank(wareId)) {
            queryWrapper.eq(WareSkuEntity::getWareId, wareId);
        }
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        //查找是否有记录，没有新增，存在修改
        LambdaQueryWrapper<WareSkuEntity> queryWrapper = new LambdaQueryWrapper<WareSkuEntity>().eq(WareSkuEntity::getSkuId, skuId).eq(WareSkuEntity::getWareId, wareId);
        WareSkuEntity dbRes = wareSkuDao.selectOne(queryWrapper);
        if (Objects.isNull(dbRes)) {
            WareSkuEntity skuEntity = new WareSkuEntity();
            skuEntity.setSkuId(skuId);
            skuEntity.setWareId(wareId);
            skuEntity.setStock(skuNum);
            skuEntity.setStockLocked(0);
            //远程调用设置skuName
            //
            try {
                R result = productFeginService.info(skuId);
                Map<String, Object> data = (Map<String, Object>) result.get("skuInfo");
                if (result.getCode() == 0) {
                    skuEntity.setSkuName((String) data.get("skuName"));
                }
            } catch (Exception e) {
                log.error("调用远程product.info服务失败,e={}", e);
            }
            skuEntity.setSkuName("");
            wareSkuDao.insert(skuEntity);

        } else {
            wareSkuDao.addStock(skuId, wareId, skuNum);
        }

    }

    @Override
    public List<SkuHasStockVO> getSkusHasStock(List<Long> skuIds) {
        List<SkuHasStockVO> skuVoList = skuIds.stream().map(skuId -> {
            SkuHasStockVO skuHasStockVO = new SkuHasStockVO();
            //查询当前库存量
            Long count = baseMapper.getSkuStock(skuId);
            skuHasStockVO.setSkuId(skuId);
            skuHasStockVO.setHasStock(count == null ? false : count > 0);
            return skuHasStockVO;

        }).collect(Collectors.toList());
        return skuVoList;
    }

}