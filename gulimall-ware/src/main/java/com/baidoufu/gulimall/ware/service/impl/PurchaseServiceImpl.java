package com.baidoufu.gulimall.ware.service.impl;

import com.baidoufu.common.constant.WareConstant;
import com.baidoufu.common.exception.RRException;
import com.baidoufu.gulimall.ware.dto.PurchaseDoneDTO;
import com.baidoufu.gulimall.ware.dto.PurchaseItemDoneDTO;
import com.baidoufu.gulimall.ware.dto.PurchaseMergeDTO;
import com.baidoufu.gulimall.ware.entity.PurchaseDetailEntity;
import com.baidoufu.gulimall.ware.service.PurchaseDetailService;
import com.baidoufu.gulimall.ware.service.WareSkuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.ware.dao.PurchaseDao;
import com.baidoufu.gulimall.ware.entity.PurchaseEntity;
import com.baidoufu.gulimall.ware.service.PurchaseService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;


@Service("purchaseService")
@Slf4j
@RequiredArgsConstructor
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {


    private final PurchaseDetailService purchaseDetailService;

    private final WareSkuService wareSkuService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageUnreceivePurchase(Map<String, Object> params) {
        LambdaQueryWrapper<PurchaseEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.
                eq(PurchaseEntity::getStatus, WareConstant.PurchaseStatusEnum.CREATE.getCode()).
                or().
                eq(PurchaseEntity::getStatus, WareConstant.PurchaseStatusEnum.ASSIGNED.getCode());
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void mergePurchase(PurchaseMergeDTO mergeDTO) {

        if (CollectionUtils.isEmpty(mergeDTO.getItems())) {
            throw new RRException("请选择要合并的采购需求单");
        }
        Long purchaseId = mergeDTO.getPurchaseId();
        //当不存在采购需求单，也就是新建一个新的采购需求单
        if (Objects.isNull(purchaseId)) {
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setUpdateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatusEnum.CREATE.getCode());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }

        //todo  采购单存在，判断是否已经开始采购了
        Long finalPurchaseId = purchaseId;
        List<PurchaseDetailEntity> purchaseDetailEntities = mergeDTO.getItems().stream().map(item -> {
            PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
            detailEntity.setId(item);
            detailEntity.setPurchaseId(finalPurchaseId);
            detailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.ASSIGNED.getCode());
            return detailEntity;
        }).collect(Collectors.toList());
        //批量保存采购需求单
        purchaseDetailService.updateBatchById(purchaseDetailEntities);
        //修改采购单的时间
        PurchaseEntity entity = new PurchaseEntity();
        entity.setId(purchaseId);
        entity.setUpdateTime(new Date());
        this.updateById(entity);

    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void received(List<Long> ids) {
        //确认当前采购单是新建或者分配状态
        Collection<PurchaseEntity> purchaseEntities = this.listByIds(ids);
        List<PurchaseEntity> entityList = purchaseEntities.stream().filter(item -> {
            if (item.getStatus().equals(WareConstant.PurchaseStatusEnum.CREATE.getCode()) ||
                    item.getStatus().equals(WareConstant.PurchaseStatusEnum.ASSIGNED.getCode())) {
                return true;
            }
            return false;
        }).map(item -> {
            item.setStatus(WareConstant.PurchaseStatusEnum.RECEIVE.getCode());
            item.setUpdateTime(new Date());
            return item;
        }).collect(Collectors.toList());

        //1、改变采购单状态
        this.updateBatchById(entityList);
        //2、改变采购需求
        entityList.forEach(item -> {
            List<PurchaseDetailEntity> detailEntityList = purchaseDetailService.listDetailByPurchaseId(item.getId());
            List<PurchaseDetailEntity> updateStatusList = detailEntityList.stream().map(detail -> {
                PurchaseDetailEntity updateEntity = new PurchaseDetailEntity();
                updateEntity.setId(detail.getId());
                updateEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.BUYING.getCode());
                return updateEntity;
            }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(updateStatusList);
        });
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void done(PurchaseDoneDTO doneDTO) {
        //采购单id
        Long purchaseId = doneDTO.getId();

        //改变采购项状态
        //用来代表采购单成功还是失败标志位
        Boolean flag = true;
        List<PurchaseItemDoneDTO> items = doneDTO.getItems();
        ArrayList<PurchaseDetailEntity> updates = Lists.newArrayList();
        for (PurchaseItemDoneDTO item : items) {
            PurchaseDetailEntity detailEntity = new PurchaseDetailEntity();
            if (WareConstant.PurchaseDetailStatusEnum.HASERROR.getCode().equals(item.getStatus())) {
                flag = false;
                detailEntity.setStatus(item.getStatus());
            } else {
                detailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.FINISH.getCode());
                PurchaseDetailEntity dbDetail = purchaseDetailService.getById(item.getItemId());
                //将成功采购的进入入库
                wareSkuService.addStock(dbDetail.getSkuId(),dbDetail.getWareId(),dbDetail.getSkuNum());
            }
            detailEntity.setId(item.getItemId());
            updates.add(detailEntity);
        }
        purchaseDetailService.updateBatchById(updates);
        //改变采购单的状态
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setStatus(flag ? WareConstant.PurchaseStatusEnum.FINISH.getCode() : WareConstant.PurchaseStatusEnum.HASERROR.getCode());
        purchaseEntity.setUpdateTime(new Date());
        this.updateById(purchaseEntity);
        //将成功采购的进入入库
    }

}