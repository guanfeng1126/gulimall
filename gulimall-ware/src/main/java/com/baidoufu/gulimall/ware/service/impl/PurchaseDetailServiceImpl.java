package com.baidoufu.gulimall.ware.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baidoufu.common.utils.PageUtils;
import com.baidoufu.common.utils.Query;

import com.baidoufu.gulimall.ware.dao.PurchaseDetailDao;
import com.baidoufu.gulimall.ware.entity.PurchaseDetailEntity;
import com.baidoufu.gulimall.ware.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {

        LambdaQueryWrapper<PurchaseDetailEntity> wrapper = new LambdaQueryWrapper<>();

        String key=(String)params.get("key");
        //skuId或者PurchaseId模糊检索
        if (StringUtils.isNotBlank(key)) {
            wrapper.and((obj)->{
                obj.eq(PurchaseDetailEntity::getPurchaseId,key).or().eq(PurchaseDetailEntity::getSkuId,key);
            });
        }
        String status=(String)params.get("status");
        if (StringUtils.isNotBlank(status)) {
            wrapper.eq(PurchaseDetailEntity::getStatus,status);
        }

        String wareId=(String)params.get("wareId");
        if (StringUtils.isNotBlank(wareId)) {
            wrapper.eq(PurchaseDetailEntity::getWareId,wareId);
        }
        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                new QueryWrapper<PurchaseDetailEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<PurchaseDetailEntity> listDetailByPurchaseId(Long purchaseId)
    {
        LambdaQueryWrapper<PurchaseDetailEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(PurchaseDetailEntity::getPurchaseId,purchaseId);
        List<PurchaseDetailEntity> purchaseDetailEntityList = this.list(queryWrapper);
        return purchaseDetailEntityList;
    }

}