package com.baidoufu.gulimall.ware.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @className: SkuHasStockVO
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 15:57
 * @version: V1.0.0
 **/
@Getter
@Setter
@ToString
public class SkuHasStockVO implements Serializable {
    private static final long serialVersionUID = -6183217790619833151L;

    private Long skuId;

    private Boolean hasStock;
}
