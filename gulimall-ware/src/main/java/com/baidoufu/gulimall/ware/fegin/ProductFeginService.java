package com.baidoufu.gulimall.ware.fegin;

import com.baidoufu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @className: ProductFeginService
 * @description:
 * @author: guanfeng
 * @date: 2020/6/6 19:05
 * @version: V1.0.0
 **/
@FeignClient("gulimall-product")
public interface ProductFeginService {
    /**
     * 远程调用 根据skuId查询sku完整信息
     * @param skuId
     * @return
     */
    @GetMapping(value = "/product/skuinfo/info/{skuId}")
    public R info(@PathVariable("skuId") Long skuId);
}
