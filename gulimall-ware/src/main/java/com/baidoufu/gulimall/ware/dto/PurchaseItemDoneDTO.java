package com.baidoufu.gulimall.ware.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @className: PurchaseItemDoneDTO
 * @description:
 * @author: guanfeng
 * @date: 2020/6/6 18:15
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
public class PurchaseItemDoneDTO implements Serializable {
    private static final long serialVersionUID = 8962809948799131357L;
    /**
     * 采购需求id
     */
    private Long itemId;
    /**
     * 采购状态
     */
    private Integer status;
    /**
     * 理由
     */
    private String reason;
}
