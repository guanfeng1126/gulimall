package com.baidoufu.gulimall.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @className: GulimallSearchApplication
 * @description:
 * @author: guanfeng
 * @date: 2020/6/14 22:23
 * @version: V1.0.0
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class GulimallSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallSearchApplication.class, args);
    }
}
