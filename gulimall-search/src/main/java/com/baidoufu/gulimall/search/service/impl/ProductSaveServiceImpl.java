package com.baidoufu.gulimall.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.baidoufu.common.to.es.SkuEsModel;
import com.baidoufu.gulimall.search.config.GulimallElasticsearchConfig;
import com.baidoufu.gulimall.search.constant.EsConstant;
import com.baidoufu.gulimall.search.service.ProductSaveService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @className: ProductSaveServiceImpl
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 17:21
 * @version: V1.0.0
 **/
@RequiredArgsConstructor
@Service
@Slf4j
public class ProductSaveServiceImpl implements ProductSaveService {

    private final RestHighLevelClient restHighLevelClient;

    @Override
    public boolean productStatusUp(List<SkuEsModel> skuEsModelList) throws IOException {
        //建立索引
        //保存映射关系

        //保存索引数据
        BulkRequest bulkRequest = new BulkRequest(EsConstant.PRODUCT_INDEX);
        for (SkuEsModel skuEsModel : skuEsModelList) {
            IndexRequest indexRequest = new IndexRequest();
            indexRequest.id(skuEsModel.getSkuId().toString());
            indexRequest.source(JSON.toJSONString(skuEsModel), XContentType.JSON);
            bulkRequest.add(indexRequest);

        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, GulimallElasticsearchConfig.COMMON_OPTIONS);

        boolean result = bulk.hasFailures();

        List<String> resultIds = Arrays.stream(bulk.getItems()).map(item -> item.getId()).collect(Collectors.toList());

        log.error("商品上架完成：{},返回数据结构{}", resultIds, result);

        return result;

    }
}
