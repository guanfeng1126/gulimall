package com.baidoufu.gulimall.search.controller;

import com.baidoufu.common.exception.BizCodeEnum;
import com.baidoufu.common.to.es.SkuEsModel;
import com.baidoufu.common.utils.R;
import com.baidoufu.gulimall.search.service.ProductSaveService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * @className: ElasticSaveController
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 17:18
 * @version: V1.0.0
 **/
@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/search/save")
public class ElasticSaveController {
    private final ProductSaveService productSaveService;

    @PostMapping("/product")
    public R productStatueUp(@RequestBody List<SkuEsModel> skuEsModelList) {
        boolean res;
        try {
            res = productSaveService.productStatusUp(skuEsModelList);
        } catch (IOException e) {
            log.error("ElasticSaveController商品上架失败,原因:{}", e);
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
        if (!res) {
            return R.ok();
        } else {
            return R.error(BizCodeEnum.PRODUCT_UP_EXCEPTION.getCode(), BizCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
    }
}
