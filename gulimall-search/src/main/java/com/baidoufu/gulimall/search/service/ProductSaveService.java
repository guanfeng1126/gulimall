package com.baidoufu.gulimall.search.service;

import com.baidoufu.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @className: ProductSaveService
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 17:20
 * @version: V1.0.0
 **/
public interface ProductSaveService {
    boolean productStatusUp(List<SkuEsModel> skuEsModelList) throws IOException;
}
