package com.baidoufu.gulimall.search.constant;

/**
 * @className: EsConstant
 * @description:
 * @author: guanfeng
 * @date: 2020/6/17 17:23
 * @version: V1.0.0
 **/
public class EsConstant {
    /**
     * product 商品索引名称
     */
    public static final String PRODUCT_INDEX="product";
}
