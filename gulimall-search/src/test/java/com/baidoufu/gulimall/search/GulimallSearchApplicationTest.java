package com.baidoufu.gulimall.search;

import com.alibaba.fastjson.JSON;
import com.baidoufu.gulimall.search.config.GulimallElasticsearchConfig;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * @className: GulimallSearchApplicationTest
 * @description:
 * @author: guanfeng
 * @date: 2020/6/14 23:02
 * @version: V1.0.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class GulimallSearchApplicationTest {

    @Autowired
    private RestHighLevelClient client;


    @Test
    public void searchData() throws IOException {
        //创建检索请求
        SearchRequest searchRequest = new SearchRequest();
        //指定索引
        searchRequest.indices("bank");

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        searchRequest.source(sourceBuilder);
        //构造检索条件
        sourceBuilder.query(QueryBuilders.matchQuery("address", "mill"));
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        sourceBuilder.aggregation(ageAgg);
        AvgAggregationBuilder balanceAgg = AggregationBuilders.avg("balanceAvg").field("balance");
        sourceBuilder.aggregation(balanceAgg);
        log.info("查询条件{}", sourceBuilder.toString());


        //z执行检索
        SearchResponse searchResponse = client.search(searchRequest, GulimallElasticsearchConfig.COMMON_OPTIONS);
        SearchHits responseHits = searchResponse.getHits();
        SearchHit[] searchHits = responseHits.getHits();

        for (SearchHit hit : searchHits) {
            String index = hit.getIndex();
            String id = hit.getId();

            String source = hit.getSourceAsString();
            Account account = JSON.parseObject(source, Account.class);

            System.out.println(account.toString());
        }

        Aggregations aggregations = searchResponse.getAggregations();

        Terms ageAgg1 = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : ageAgg1.getBuckets()) {
            String key = bucket.getKeyAsString();
            System.out.println("年龄+" + key + "人数：" + bucket.getDocCount());
        }


        //分析结果
        log.info("查询结果：{}", searchResponse.toString());

    }


    @Test
    public void testRestHighLevelClient() {

        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1");
        User user = new User();
        user.setAge(18);
        user.setUserName("zhangsan");
        user.setGender("男");
        String jsonString = JSON.toJSONString(user);
        indexRequest.source(jsonString, XContentType.JSON);

        try {
            IndexResponse index = client.index(indexRequest, GulimallElasticsearchConfig.COMMON_OPTIONS);
            log.info("es返回数据内容：{}", index);
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("client={}", client);
    }

    class User {
        private String userName;

        private Integer age;
        private String gender;

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }

    @Data
    static class Account {

        private int account_number;
        private int balance;
        private String firstname;
        private String lastname;
        private int age;
        private String gender;
        private String address;
        private String employer;
        private String email;
        private String city;
        private String state;

    }

}

