package com.baidoufu.gulimall.thirdparty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @className: GulimallThirdPartyApplication
 * @description:
 * @author: guanfeng
 * @date: 2020/5/14 19:01
 * @version: V1.0.0
 **/
@SpringBootApplication
@EnableDiscoveryClient
public class GulimallThirdPartyApplication {
    public static void main(String[] args) {
        SpringApplication.run(GulimallThirdPartyApplication.class,args);
    }
}
