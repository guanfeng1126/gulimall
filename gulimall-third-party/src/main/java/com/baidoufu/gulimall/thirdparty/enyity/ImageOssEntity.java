package com.baidoufu.gulimall.thirdparty.enyity;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @className: ImageOssEntity
 * @description: 包装返回阿里oss 后端只传服务
 * @author: guanfeng
 * @date: 2020/5/14 19:31
 * @version: V1.0.0
 **/
@Setter
@Getter
@ToString
public class ImageOssEntity {

    private String accessId;

    private String policy;

    private String signature;

    private String dir;

    private String host;

    private String expire;

}
