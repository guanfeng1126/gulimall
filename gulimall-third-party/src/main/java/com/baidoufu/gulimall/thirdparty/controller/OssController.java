package com.baidoufu.gulimall.thirdparty.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.baidoufu.common.utils.R;
import com.baidoufu.gulimall.thirdparty.enyity.ImageOssEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @className: OssController
 * @description:
 * @author: guanfeng
 * @date: 2020/5/14 19:28
 * @version: V1.0.0
 **/
@RestController
@RequiredArgsConstructor
public class OssController {

    private final OSS ossClient;

    @Value("${spring.cloud.alicloud.oss.endpoint}")
    private String endpoint;
    @Value(("${spring.cloud.alicloud.oss.bucket}"))
    private String bucket;
    @Value("${spring.cloud.alicloud.access-key}")
    private String accessId;

    @GetMapping("/oss/policy")
    public R policy() {
        // host的格式为 bucketname.endpoint
        String host = "https://" + bucket + "." + endpoint;
        // callbackUrl为 上传回调服务器的URL，请将下面的IP和Port配置为您自己的真实信息。
        //String callbackUrl = "http://88.88.88.88:8888";
        LocalDate now = LocalDate.now();
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String format = now.format(fmt);
        // 用户上传文件时指定的前缀。
        String dir = format ;

        // 创建OSSClient实例。
        // OSS ossClient = new OSSClientBuilder().build(endpoint, accessId, accessKey);
        ImageOssEntity ossEntity = new ImageOssEntity();
        try {
            long expireTime = 30;
            long expireEndTime = System.currentTimeMillis() + expireTime * 1000;
            Date expiration = new Date(expireEndTime);
            // PostObject请求最大可支持的文件大小为5 GB，即CONTENT_LENGTH_RANGE为5*1024*1024*1024。
            PolicyConditions policyConds = new PolicyConditions();
            policyConds.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConds.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, dir);

            String postPolicy = ossClient.generatePostPolicy(expiration, policyConds);
            byte[] binaryData = postPolicy.getBytes("utf-8");
            String encodedPolicy = BinaryUtil.toBase64String(binaryData);
            String postSignature = ossClient.calculatePostSignature(postPolicy);

            ossEntity.setAccessId(accessId);
            ossEntity.setPolicy(encodedPolicy);
            ossEntity.setSignature(postSignature);
            ossEntity.setDir(dir);
            ossEntity.setHost(host);
            ossEntity.setExpire(String.valueOf(expireEndTime / 1000));

//            Map<String, String> respMap = new LinkedHashMap<String, String>();
//            respMap.put("accessid", accessId);
//            respMap.put("policy", encodedPolicy);
//            respMap.put("signature", postSignature);
//            respMap.put("dir", dir);
//            respMap.put("host", host);
//            respMap.put("expire", String.valueOf(expireEndTime / 1000));
//            return respMap;
            // respMap.put("expire", formatISO8601Date(expiration));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return R.ok(ossEntity);

    }
}
