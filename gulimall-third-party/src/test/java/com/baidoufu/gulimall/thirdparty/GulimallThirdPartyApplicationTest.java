package com.baidoufu.gulimall.thirdparty;

import com.aliyun.oss.OSSClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * @className: GulimallThirdPartyApplicationTest
 * @description:
 * @author: guanfeng
 * @date: 2020/5/14 19:19
 * @version: V1.0.0
 **/
@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallThirdPartyApplicationTest {
    @Autowired
    private OSSClient ossClient;
    @Test
    public void testUploadByoss() throws FileNotFoundException {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        //String endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
// 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        // String accessKeyId = "<yourAccessKeyId>";
        //String accessKeySecret = "<yourAccessKeySecret>";

// 创建OSSClient实例。
        // OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

// 上传文件流。
        InputStream inputStream = new FileInputStream("D:\\tools\\image\\feiqiu.jpg");
        ossClient.putObject("gulimall-brand", "feiqiu123.jpg", inputStream);

// 关闭OSSClient。
        //ossClient.shutdown();
        System.out.println("上传完毕。。。");
    }
}
